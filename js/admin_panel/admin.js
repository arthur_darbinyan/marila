var isAdmin = true;

function ImagesAdmin() {
	
	this.removeImage = function(imgId, _page) {
		$.ajax({
			type: "POST",
			url: "admin_panel/actions_controller.php",
			data: { action: "RemoveImage", imgId: imgId, page: _page }
		}).done(function( msg ) {
			var view;
			if(_page=="company") {
				view = imagesCompanyView;
			} else {
				view = imagesMeetingsView;
			}
			view.getImages().done(function(data) {
				view.renderImages(data);
				$(".group1").colorbox({rel:'group1', maxWidth: "85%", maxHeight: "90%"});
			});
		}).fail(function(jqXHR, textStatus) {
			alert( "Request failed: " + textStatus );
		});
	},
	
	this.renderAdminUI = function(page) {
		
		$("#a_ui").append("<div class='page_content_start_line' style='background: #F1484A'></div>"
		    + "<form action='' onsubmit='return ImagesAdmin.validateImages()' method='post' enctype='multipart/form-data' style='margin-top: 10px; float: left'>"
		    + "<table><tr><td><span>Small Image(300 X 250)</span></td>"
			+ "<td><input type='file' accept='image/*' id='new-image-small' name='image-small' style='margin-left: 10px'></td></tr>"
			+ "<tr style='margin-top: 5px'><td><span>Big Image(any size)</span></td>"
			+ "<td><input type='file' accept='image/*' id='new-image-big' name='image-big' style='margin-left: 10px'></td></tr>"
			+ "<tr><td><input type='submit' style='padding: 3px; padding-left: 6px; padding-right: 6px'"
			+ "value='Upload' name='action'/>"
			+ "</td><td><input type='hidden' name='page' value='" + page + "'></td></tr></table>"
			+ "</form>");
		
		var url = window.URL || window.webkitURL;	
		$("#new-image-small").change(function(e) {
			var self = $(this);
		    if( this.disabled ){
		        alert('Your browser does not support File upload.');
		    }else{
		        var chosen = this.files[0];
		        var image = new Image();
		        image.onload = function() {
		        	if(this.width != 300 || this.height != 250) {
		        		alert("Invalid image size - " + 'Width: '+ this.width +'px, Height: '+ this.height + "px\n"
		        		+ "Image size should be - Width: 300px, Height: 250px" );
		        		self.val("")
		        	}
		        };
		        image.onerror = function() {
		            alert('Not a valid file type: '+ chosen.type);
		            self.val("");
		        };
		        image.src = url.createObjectURL(chosen);                    
		     }
		});
	}
	
};

ImagesAdmin.validateImages = function() {

	if($('#new-image-small').val() == '' || $('#new-image-big').val() == '') {
		alert("Please upload images");
		return false;
	} else if(!isImage($('#new-image-small').val()) || ! isImage($('#new-image-big').val())) {
		alert("Invalid Files: Please upload png or jpg images");
		return false;
	}
	
	return true;
};

ImagesAdmin.addAdminUI = function(img, imgID, _page) {
	var deleteIcon = $("<img src='img/remove.png' class='delete_img'/>");
	var page = _page;
	
	deleteIcon.click(function(event) {
		event.stopPropagation();
		var remove = confirm("Delete Image?");
		if(remove) {
			imagesAdmin.removeImage(imgID, page);
		}
	});

	$(img).append(deleteIcon);
};

var imagesAdmin = new ImagesAdmin();


// products

function ProductsAdmin() {
	
};


ProductsAdmin.removeProductImage = function() {
	
};


ProductsAdmin.renderAddProductButton = function(categoryId, productType) {

	$("#a_ui").append("<input type='button' id='add_product' class='red_gradient admin_add_button' value='Add Product' />");
	$("#add_product").click(function() {
    
		$("#popup_content").html("");
		$('#products_popup').bPopup({});
		
		loadTemplate("templates/admin_panel/AddProduct.html").done(function(html) {
			$("#popup_content").html(Mustache.to_html(html, {
                data: {},
                lang: window.lang
            }));
            
            $('.new_product_description').autosize();
            ProductsAdmin.renderSectionRemoveIcons();
            ProductsAdmin.initializeListeners();
            ProductsAdmin.setLanguage();
            
            $("#save_product").click(function() {
            	ProductsAdmin.addNewProduct(categoryId, productType);
            });
		});
	});
};

ProductsAdmin.getProductJson = function(imageChanged) {
	var json = {
		"arm": {},
		"en": {},
		"ru": {},
	};
	//json["category"] = categoryId;
	// armenian description
	$("#new_product_arm .section").each(function(index, section) {
		json.arm.sectionsLength = index + 1;
		json.arm["section" + index] = [];
		
		$(section).find("input[data-type='section-row']").each(function(_index, input) {
			json.arm["section" + index].push({'val' : input.value});
		});
	});
	
	$("#new_product_arm input[data-type='product-title']").each(function(index, input) {
		json.arm.title = input.value;
	});
	
	$("#new_product_arm .new_product_description").each(function(index, textarea) {
		json.arm.description = $(textarea).val();//.replace(/<br\s?\/?>/g,"\n");
		
	});
	
	// english description
	$("#new_product_en .section").each(function(index, section) {
		json.en.sectionsLength = index + 1;
		json.en["section" + index] = [];
		
		$(section).find("input[data-type='section-row']").each(function(_index, input) {
			json.en["section" + index].push({'val' : input.value});
		});
	});
	
	$("#new_product_en input[data-type='product-title']").each(function(index, input) {
		json.en.title = input.value;
	});
	
	$("#new_product_en .new_product_description").each(function(index, textarea) {
		json.en.description = $(textarea).val();
	});
	
	
	// russian description
	$("#new_product_ru .section").each(function(index, section) {
		json.ru.sectionsLength = index + 1;
		json.ru["section" + index] = [];
		
		$(section).find("input[data-type='section-row']").each(function(_index, input) {
			json.ru["section" + index].push({'val' : input.value});;
		});
	});
	
	$("#new_product_ru input[data-type='product-title']").each(function(index, input) {
		json.ru.title = input.value;
	});
	
	$("#new_product_ru .new_product_description").each(function(index, textarea) {
		json.ru.description = $(textarea).val();
	});
	
	if(!imageChanged) {
		json.image = $("#edit_img").attr("src");
	}
	
	return json;
};

ProductsAdmin.validateProduct = function(json) {
	
	var langs = ['arm', 'en', 'ru'];
	var langsfull = ['armenian', 'english', 'russian'];
	var error = "";
	$.each(langs, function(index, lang) {
		
		if($.trim(json[lang].title) == "") {
			error += "Please fill " + langsfull[index] + " title\n";
		}
		if($.trim(json[lang].description) == "") {
			error += "Please fill " + langsfull[index] + " description\n";
		}
	});
	
	if($("#new_product_image").length >0) {
		if($("#new_product_image").val()=="") {
			error += "Please upload product image\n";
		}
		else if(!isImage($("#new_product_image").val())) {
			error += "Invalid product image file\n";
		}
	}
	
	if(error!="") {
		alert(error);
		return false;
	}
	return true;
};

ProductsAdmin.addNewProduct = function(categoryId, productType) {
	var json = ProductsAdmin.getProductJson(true);
	if(!ProductsAdmin.validateProduct(json)) {
		return false;
	}
	
	$("#new_product_description").val(JSON.stringify(json));
	$("#new_product_category").val(categoryId);
	$("#new_product_type").val(productType);

	add_product_form.submit();
};

ProductsAdmin.saveEditedProduct = function(product_info, productType, imageChanged) {
	var json = ProductsAdmin.getProductJson(imageChanged);
	if(!ProductsAdmin.validateProduct(json)) {
		return false;
	}
	$("#edit_product_type").val(productType);
	$("#edit_product_info").val(JSON.stringify(product_info));
	$("#edit_product_description").val(JSON.stringify(json));
	edit_product_form.submit();
};

ProductsAdmin.setLanguage = function() {
	ProductsAdmin.currentLanguage = window.currentLanguage;
	$("#admin_language_product ." + ProductsAdmin.currentLanguage + ":first").addClass("selected");
	$("#new_product_" + ProductsAdmin.currentLanguage).css("display", "block");
	
	$("#admin_language_product").delegate("span", "click", function() {
		
		$("#admin_language_product ." + ProductsAdmin.currentLanguage + ":first").removeClass("selected");
		$(this).addClass("selected");
		$("#new_product_" + ProductsAdmin.currentLanguage).css("display", "none");
		$("#new_product_" + $(this).attr("data-lang")).css("display", "block");
		ProductsAdmin.currentLanguage = $(this).attr("data-lang");
	});
};

ProductsAdmin.initializeListeners = function() {
	this.initializeAddRowListeners();
};

ProductsAdmin.initializeAddRowListeners = function() {
	$(".add_row_img").each(function() {
		$(this).click(function() {
			var row = $('<div><input type="text" data-type="section-row" value="" style="width: 285px; margin-top: 3px"/>'
			+ '<img src="img/remove.png" class="remove_row_img"></div>');
			row.insertBefore($(this));
			
			$(row).find("img:first").click(function() {
				$(this).parent().remove();
			});
		});
	});
	
	$(".remove_row_img").each(function() {
		$(this).click(function(){
			$(this).prev().remove();
			$(this).remove();
		});
	});
};

ProductsAdmin.renderRemoveEditButtons = function(productDiv, product, categoryId, productType) {
	var imagesDiv = $("<div style='position: absolute; margin-left: 180px; margin-top: 8px'></div>");
	var editImage = $("<img src='img/edit.png' title='Edit' style='margin-right: 10px'/>");
	var deleteImage = $("<img src='img/delete.png' title='Delete' style='margin-right: 5px; width: 22px'/>");
	imagesDiv.insertBefore($(productDiv).find(".product_item_title:first"));
	imagesDiv.append(editImage);
	imagesDiv.append(deleteImage);
	
	editImage.click(function(event) {
		event.stopPropagation();
		ProductsAdmin.editProduct(product, categoryId, productType);
	});
	
	deleteImage.click(function(event) {
		event.stopPropagation();
		var remove = confirm("Delete Product?");
		if(remove) {
			ProductsAdmin.deleteProduct(product, productType);
		}
	});
};

ProductsAdmin.deleteProduct = function(product, productType) {
	$.ajax({
		type: "POST",
		url: "admin_panel/actions_controller.php",
		data: { action: "DeleteProduct", productId: product.id, productType: productType}
	}).done(function( msg ) {
		$("#product" + product.id).remove();
	}).fail(function(jqXHR, textStatus) {
		alert( "Request failed: " + textStatus );
	});
};

ProductsAdmin.editProduct = function(product, categoryId, productType) {
	
	$("#popup_content").html("");
	$('#products_popup').bPopup({});
	var removeImage = false, imageChanged = false;
	
	$.getJSON(product.dataDetails, function(_data) {
			
		loadTemplate("templates/admin_panel/EditProduct.html").done(function(html) {
		//	_data.dataIcon = product["dataIcon"];
			
			$("#popup_content").html(Mustache.to_html(html, {
	            data: _data,
	            lang: window.lang
	        }));
	      
	        $("#edit_img").load(function() {
	        	$("#edit_img").css("margin-left", (380 - parseInt($(this).width())) / 2 - 10 + "px");
	        	
		        $("#delete_product_img").css("left", (380 - parseInt( $("#delete_product_img").width())) / 2 - 10 + "px");
		        $("#delete_product_img").css("top", "100px");
	        });
			
			$("#delete_product_img").click(function() {
				$("#delete_product_img").parent().css("display", "none");
				// $('<div>Change Image</div>').insertBefore($("#new_product_description"));
				$('<input type="file" id="new_product_image" accept="image/*" name="change_product_image"/>')
					.insertBefore($("#edit_product_description"));
				// AdminUtils.removeImage(product["dataIcon"]);
				removeImage = true;
				imageChanged = true;
			});
	        
	        $('.new_product_description').focus(function() {
	        	$('.new_product_description').autosize();
	        });
	       
	        ProductsAdmin.renderSectionRemoveIcons();
	        ProductsAdmin.initializeListeners();
	        ProductsAdmin.setLanguage();
	         
	        $("#save_product").click(function() {
	        	ProductsAdmin.saveEditedProduct({'categoryId': categoryId, 'productId': product.id}, productType, imageChanged);
	        	if(removeImage) {
	        		//AdminUtils.removeImage(product["dataIcon"]);
	        	}
	        });
		});
	});
};

ProductsAdmin.renderSectionRemoveIcons = function() {
	
};

ProductsAdmin.renderAddProductUI = function() {
	$("#a_ui").append("<input  type='button' class='red_gradient admin_add_button' value='Add Product' />");
};



function CareersAdmin() {
	
};

CareersAdmin.initializeView = function() {
	
	StaticPageAdmin.setContentEditable($("#text_content"), 646, 100);
	CareersAdmin.renderButtons();
};

CareersAdmin.renderButtons = function() {
	if($("#add_vacancy").length) {
		return;
	}
	$("#careers").parent().append("<input type='button' id='add_vacancy' style='margin-top: 30px'"
	+ " class='red_gradient admin_add_button' value='Add Vacancy' />");
	
	$("#add_vacancy").click(function() {
		CareersAdmin.showAddVacancyPopup();
	});
	
	var saveTextBtn = $("<input type='button' id='save_text' style='margin-top: 30px'" +  
		"class='red_gradient admin_add_button' value='Save Text' />");
	$("#save_text_div").append(saveTextBtn);
	
	saveTextBtn.click(function() {
		$.ajax({
			type: "POST",
			url: "admin_panel/actions_controller.php",
			data: { 
				action: "SaveStaticPageText", 
				page: window.currentpage, 
				language: Localization.currentLanguage(), 
				text: nicEditors.findEditor('myArea').getContent()
			}
		}).done(function( msg ) {
			alert("Succesfully saved");
		}).fail(function(jqXHR, textStatus) {
			alert( "Request failed: " + textStatus );
		});
	});
};

CareersAdmin.renderRemoveVacancyButton = function(itemDiv) {
	var removeVacancyImg = $("<img src='img/remove.png'"
	+ "style='width: 16px; position: relative; cursor: pointer; margin-left: 20px; top: 3px'/>");
	$(itemDiv).append(removeVacancyImg);
	
	removeVacancyImg.click(function() {
		var remove = confirm("Delete Vacancy?");
		if(remove) {
			CareersAdmin.deleteVacancy($(itemDiv).attr("data-id"));
		}
	});
};

CareersAdmin.showAddVacancyPopup = function() {
	
	loadTemplate("templates/admin_panel/AddVacancy.html").done(function(html) {
		
		$("#career_popup_content").html(Mustache.to_html(html, {
            data: {},
            lang: window.lang
        }));
        
        $('.new_job_responsibilities').autosize();
        $('.new_product_description').autosize();
        //$('.new_product_description').autosize();
        CareersAdmin.setLanguage();
        $('#career_popup').bPopup({
        	onClose: function() {
        		$('#career_popup_content').html("");
            }
        });
        $("#save_vacancy").click(function() {
        	CareersAdmin.addVacancy();
        });
	});
};

CareersAdmin.setLanguage = function() {
	CareersAdmin.currentLanguage = window.currentLanguage;
	$("#admin_language ." + CareersAdmin.currentLanguage + ":first").addClass("selected");
	$("#new_vacancy_" + CareersAdmin.currentLanguage).css("display", "block");
	
	$("#admin_language").delegate("span", "click", function() {
		
		$("#admin_language ." + CareersAdmin.currentLanguage + ":first").removeClass("selected");
		$(this).addClass("selected");
		$("#new_vacancy_" + CareersAdmin.currentLanguage).css("display", "none");
		$("#new_vacancy_" + $(this).attr("data-lang")).css("display", "block");
		CareersAdmin.currentLanguage = $(this).attr("data-lang");
	});
};

CareersAdmin.getCareersJson = function() {
	var json = {
		"arm": {},
		"en": {},
		"ru": {},
	};
	
	json.arm['title'] = $("#new_vacancy_arm .new_job_title:first").val();
	json.arm['responsibilities'] = $("#new_vacancy_arm .new_job_responsibilities:first").val();
	json.arm['description'] = $("#new_vacancy_arm .new_product_description:first").val();
	
	json.en['title'] = $("#new_vacancy_en .new_job_title:first").val();
	json.en['responsibilities'] = $("#new_vacancy_en .new_job_responsibilities:first").val();
	json.en['description'] = $("#new_vacancy_en .new_product_description:first").val();
	
	json.ru['title'] = $("#new_vacancy_ru .new_job_title:first").val();
	json.ru['responsibilities'] = $("#new_vacancy_ru .new_job_responsibilities:first").val();
	json.ru['description'] = $("#new_vacancy_ru .new_product_description:first").val();

	return json;		
};

CareersAdmin.addVacancy = function() {
	var json = CareersAdmin.getCareersJson();
	for(var i in json) {
		for(var j in json[i]) {
			if($.trim(json[i][j])=="") {
				alert("Please fill all requred filds");
				return;
			}
		}
	}
	$.ajax({
		type: "POST",
		url: "admin_panel/actions_controller.php",
		data: { action: "AddVacancy", data: JSON.stringify(json)}
	}).done(function( msg ) {
		$('#career_popup').bPopup().close();
		careersView.getCareers().done(function(careers) {
			careersView.renderCareers(careers);
		});
	}).fail(function(jqXHR, textStatus) {
		alert( "Request failed: " + textStatus );
	});
};

CareersAdmin.deleteVacancy = function(vacancyId) {
	$.ajax({
		type: "POST",
		url: "admin_panel/actions_controller.php",
		data: { action: "DeleteVacancy", vacancyId: vacancyId }
	}).done(function( msg ) {
		careersView.getCareers().done(function(careers) {
			careersView.renderCareers(careers);
		});
	}).fail(function(jqXHR, textStatus) {
		alert( "Request failed: " + textStatus );
	});
};
