function AdminContactUs() {

};

AdminContactUs.initializeView = function(map, markers) {
	
	StaticPageAdmin.setContentEditable($("#text_content"), 460, 100);
	
	var saveTextBtn = $("<input type='button' style='margin-top: 30px'" +  
		"class='red_gradient admin_add_button' value='Save Text' />");
	$("#save_text_div").append(saveTextBtn);
	
	saveTextBtn.click(function() {
		$.ajax({
			type: "POST",
			url: "admin_panel/actions_controller.php",
			data: { 
				action: "SaveStaticPageText", 
				page: window.currentpage, 
				language: Localization.currentLanguage(), 
				text: nicEditors.findEditor('myArea').getContent()
			}
		}).done(function( msg ) {
			alert(msg);
		}).fail(function(jqXHR, textStatus) {
			alert( "Request failed: " + textStatus );
		});
	});
	
	google.maps.event.addListener(map, "rightclick", function(event) {
		var lat = new Number(event.latLng.lat()).toPrecision(9);
		var lng = new Number(event.latLng.lng()).toPrecision(9);
  
	    var add = confirm("Add New Market?");
		if(add) {
			AdminUtils.addMarker({ 
					position: new google.maps.LatLng(lat, lng),
				 	map: map, title: "Market" 
				 }, lat, lng);
		}
	});
	
	$.each(markers, function(i, marker) {
		google.maps.event.addListener(marker, "rightclick",function() {
	   		var lat = new Number(this.position.lat()).toPrecision(9);
			var lng = new Number(this.position.lng()).toPrecision(9);
	    
			var remove = confirm("Remove Market?");
			if(remove) {
				AdminUtils.removeMarker(marker, lat, lng);
			}
		});
	});
	
	var saveMap = $("<div><input type='button' class='red_gradient admin_add_button' style='margin-top: 15px' value='Save Map' /><div>");
	$("#map_container").append(saveMap);
	
	saveMap.click(function() {
		var lat = new Number(map.getCenter().lat()).toPrecision(9);
		var lng = new Number(map.getCenter().lng()).toPrecision(9);
			
		AdminUtils.saveMap(map.getZoom(), lat, lng);
	});
}