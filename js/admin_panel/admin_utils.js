function AdminUtils() {

};

AdminUtils.addMarker = function(markerData, lat, lng) {
	$.ajax({
		type: "POST",
		url: "admin_panel/actions_controller.php",
		data: { action: "AddMarker", lat: lat, lng: lng, page: window.currentpage }
	}).done(function( msg ) {
		alert(msg);
		var newMarker = new google.maps.Marker(markerData);
		google.maps.event.addListener(newMarker, "rightclick",function() {
			var remove = confirm("Remove Market?");
			if(remove) {
				AdminUtils.removeMarker(newMarker, lat, lng);
			}
		});
	}).fail(function(jqXHR, textStatus) {
		alert( "Request failed: " + textStatus );
	});
};

AdminUtils.removeMarker = function(marker, lat, lng) {
	$.ajax({
		type: "POST",
		url: "admin_panel/actions_controller.php",
		data: { action: "RemoveMarker", lat: lat, lng: lng, page: window.currentpage }
	}).done(function( msg ) {
		alert(msg);
		marker.setMap(null);
	}).fail(function(jqXHR, textStatus) {
		alert( "Request failed: " + textStatus );
	});
};

AdminUtils.saveMap = function(zoom, lat, lng) {
	$.ajax({
		type: "POST",
		url: "admin_panel/actions_controller.php",
		data: { action: "SaveMap", centerLat: lat, centerLng: lng, zoom: zoom, page: window.currentpage }
	}).done(function( msg ) {
		alert(msg);
	}).fail(function(jqXHR, textStatus) {
		alert( "Request failed: " + textStatus );
	});
};
