function initLanguages() {
	var cacheValue = cache.getItem(Localization.cacheKey);
	window.currentLanguage = cacheValue ? cacheValue : Localization.defaultLanguage;
	Localization.setLanguage(window.currentLanguage);
};

window.onload = initLanguages