var topHeaderView = new View();
topHeaderView.initialize({
	templateId: "TopHeader",
	parentElementId: "top_header_content"
});

topHeaderView.initilizeListeners = function() {
	this.initilizeView();
	$.each(Localization.languages, function(index, language) {
		$("#language_" + language).click(function() {
			$("#language_" + currentLanguage).removeClass("selected_language");
			$(this).addClass("selected_language");
			// save in storage
			Localization.setLanguage(language);
		});

		if (language === window.currentLanguage) {
			$("#language_" + language).addClass("selected_language");
		}
	});
};

topHeaderView.initilizeView = function() {
	(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	
	
	if(typeof TopHeaderAdmin != "undefined" && TopHeaderAdmin) {
		TopHeaderAdmin.showLogout();
	}
};


topHeaderView.setViewSize = function() {
	//alert("D")
	//$("#slider_img_container div:nth-child(0)").css("width", $(window).width());
	//$("#slider_img_container div:nth-child(0)").css("height", parseInt($(window).height()) - 90 + "px");
	//$("#slider_img_container div:nth-child(1)").css("width", $(window).width());
	//$("#slider_img_container div:nth-child(1)").css("height", parseInt($(window).height()) - 90 + "px");
};

