<?php
	require_once('admin_panel/upload.php');
	require_once('admin_panel/marila_api.php');
	
	$marilaApi = new MarilaApi();
		
	if(isset($_FILES['image-small']['name']) && isset($_FILES['image-big']['name'])) {
		$marilaApi->addImages(array('image-small', 'image-big'), $_POST['page']);
	}
	else
	if(isset($_POST['new_product_description'])) {
		$marilaApi->addProduct($_POST['new_product_description'], $_POST['new_product_category'], $_POST['new_product_type']);
	}
	else
	if(isset($_POST['edit_product_description'])) {
		if(isset($_FILES["change_product_image"]['name'])) {
			$marilaApi->editProduct($_POST['edit_product_type'], $_POST['edit_product_description'], $_POST['edit_product_info'], "change_product_image");
		} else {
			$marilaApi->editProduct($_POST['edit_product_type'], $_POST['edit_product_description'], $_POST['edit_product_info'], "");
		}
	} else if(isset($_POST['new_slider_data'])) {
		$marilaApi->addSlider('slider_img', $_POST['new_slider_data']);
	} else if(isset($_FILES['page_img']['name'])) {
		$marilaApi->updateStaticPageImage($_POST['language'], $_POST['page'], 'page_img');
	} else if(isset($_FILES['video_img']['name'])) {
		$data = (!isset($_POST["videoData"])) ? "" : $_POST["videoData"];
		$page = (!isset($_POST["page"])) ? "" : $_POST["page"];
		$marilaApi->addVideo($data, $page);
	}
	
?>