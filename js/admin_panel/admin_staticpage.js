function StaticPageAdmin() {
	
};

StaticPageAdmin.docClickEvents = [];

StaticPageAdmin.docClickEvent = function(element) {
	StaticPageAdmin.unbindEvents();
	var eventFunction = function(event) {
		if(event.target == $(element)[0]) {
			return;
		}
	    if ($(".nicEdit-pane:first").has(event.target).length==0 &&
	    		$(element).has(event.target).length==0 && 
	    			$("#w_div").has(event.target).length==0) {
	    	if(!nicEditors.findEditor('myArea')) {
	    		return;
	    	}
	    	$(element).html(nicEditors.findEditor('myArea').getContent());
	        $(element).css("display", "block");
			$("#w_div").css("display", "none");
	    }
	};
	StaticPageAdmin.docClickEvents.push(eventFunction);
	return eventFunction;
};

StaticPageAdmin.addHistoryImage = function() {
	var error = "";
	var imageName = $("#page_img").val();

	if(imageName=="") {
		error += "Please upload image \n";
	} else if(!isImage(imageName)) {
		error += "Invalid File: Please upload png or jpg files\n";
	}
	if(error!="") {
		alert(error);
		return false;
	}
	$("#language_hidden").val(Localization.currentLanguage());
	return true;
};

StaticPageAdmin.setContentEditable = function(element, width, minHeight) {
	$(element).click(function() {
		$("textarea").each(function() {
			if(!$(this).attr('id')) {
				$(this).remove();
			}
		});
		var self = $(this);
		$(this).css("display", "none");
		if($("#w_div").length == 0) {
			
			$('<div id="w_div" style="float: left;">' +
			'<textarea id="myArea" style="width: ' + width + 'px;' + ' min-height: ' +minHeight 
			+ 'px; padding: 0px;"></textarea></div>')
				.insertBefore($(this));
			
			nicEditors.allTextAreas({buttonList: ['bold','italic', 'underline', 'left', 'center', 'right', 'justify', 'ol', 
			'ul', 'indent', 'outdent', 'link', 'unlink']});
			
			nicEditors.findEditor('myArea').setContent($(this).html());
		}
		$("#w_div").css("display", "block");
	});
	$(document).bind('click', StaticPageAdmin.docClickEvent(element));
};

StaticPageAdmin.initializeBenefitsView = 
StaticPageAdmin.initializeHistoryView = StaticPageAdmin.initializeAchievementsView = function() {
	
	StaticPageAdmin.setContentEditable($("#text_content"), 446, 300);
	
	var save = $("<input type='button' class='red_gradient admin_add_button' style='margin-top: 20px' value='Save' />");
	$("#a_ui").append(save);
	
	var addImageForm = $("<form action='' onsubmit='return StaticPageAdmin.addHistoryImage()' method='post' enctype='multipart/form-data'>"
		+ '<div>Change Image<b>(width >= 500px)</b><input type="file" accept="image/*" style="margin-left: 10px" id="page_img" name="page_img"/></div>'
		+ '<input type="hidden" name="language" id="language_hidden"/>'
		+ '<input type="hidden" name="page" value="' + window.currentpage +  '"/>'
		+ '<input type="submit" value="Upload" class="admin_add_button red_gradient"/></form>');
	
	$("#img_form").append(addImageForm);
	
	var url = window.URL || window.webkitURL;	
	$("#page_img").change(function(e) {
		var self = $(this);
	    if( this.disabled ){
	        alert('Your browser does not support File upload.');
	    } else{
	        var chosen = this.files[0];
	        var image = new Image();
	        image.onload = function() {
	        	if(this.width < 500) {
	        		alert("Invalid image size - " + 'Width: '+ this.width +'px, Height: '+ this.height + "px\n"
	        		+ "Image width should be greater or equal than 500px" );
	        		self.val("")
	        	}
	        };
	        image.onerror = function() {
	            alert('Not a valid file type: '+ chosen.type);
	            self.val("");
	        };
	        image.src = url.createObjectURL(chosen);                    
	     }
	});
	
	save.click(function() {
		$.ajax({
			type: "POST",
			url: "admin_panel/actions_controller.php",
			data: { 
				action: "SaveStaticPageText", 
				page: window.currentpage, 
				language: Localization.currentLanguage(), 
				text: nicEditors.findEditor('myArea').getContent()
			}
		}).done(function( msg ) {
			alert(msg);
		}).fail(function(jqXHR, textStatus) {
			alert( "Request failed: " + textStatus );
		});
	});
};

StaticPageAdmin.unbindEvents = function() {
	$.each(StaticPageAdmin.docClickEvents, function(index, event) {
		$(document).unbind('click', event);
	});
};
