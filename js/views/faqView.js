var faqView = new View();
faqView.initialize({
	templateId: "Faq",
	parentElementId: "page_content",
	templateDependsOnLanguage: true
});

faqView.initilizeView = function(categoryId) {
	faqView.getQuestions().done(function(questions) {
		faqView.renderQuestions(questions);
		if(typeof AdminFAQ != "undefined" && AdminFAQ) {
			AdminFAQ.initializeView();
		}
	});
};

faqView.renderQuestions = function(questions) {
	$.each(questions, function(i, question) {
		var questionDiv = $('<div class="faq_margin_top"><img src="img/FAQ-item.png" style="width: 14px">'
		+ '<span class="section_title section_title_' + Localization.currentLanguage() +  ' faq_title">'
		+ question.title + '</span><div class="faq_answer">' + question.text + "</div><div class='faq_line'></div></div>");
		
		questionDiv.find(".faq_answer:first").addClass("paragraph_" + Localization.currentLanguage());
		
		$("#faq_div").append(questionDiv);
		
		if(typeof AdminFAQ != "undefined" && AdminFAQ) {
			AdminFAQ.renderRemoveEditButtons(questionDiv, question);
		}
	});
	
	$("#faq_div .faq_line:last").remove();
};

faqView.getQuestions = function() {
	var deferred = $.Deferred();
	var questions = [];
	$.ajaxSetup({ cache: false });
	$.get('data/faqPage/content-' + Localization.currentLanguage() + '.xml', function(data) {
		$(data).find('question').each(function() {
			var $question = $(this);
			questions.push({
				id: $question.attr("id"),
				title: $question.find("title:first").text(),
				text: $question.find("text:first").text()
			});
		});
		
		deferred.resolve(questions);
		$.ajaxSetup({ cache: true });
	});
	
	return deferred;
};

faqView.initilizeListeners = function() {
	this.initilizeView();
	menuView.selectMenu();
};