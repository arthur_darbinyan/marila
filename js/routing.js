Router = function() {
	var self = this;
	this.currentPage = null;
	lastHash = null;
	self.onHashChange(window.location.hash);
	window.onhashchange = function(hash) {
		return self.onHashChange(hash);
	};
};

// Loads content for the new page hash
Router.prototype.onHashChange = function( hash ) {
    hash = window.location.hash;
    if(hash==lastHash) {
		return;
	}
	lastHash = hash;
    if ( '' === hash ) {
        hash = '#home';
    }

    //window.console.log( 'routing::onHashChange', 'Navigating to hash',  hash );
    this.changeTo( hash, getUrlHashParameters( hash ) );

    return false;
};

Router.prototype.getRouteForHash = function( hash ) {
    // Search for a route that matches the current hash
    var page = null,
    i = 0,
    routesLength = this.routes.length,
    route;
    for ( ; i < routesLength; i++ ) {
        route = this.routes[ i ];
        if ( hash.search( route.matcher ) >= 0 ) {
            page = route.page;
            break;
        }
    }

    return page;
};

// Populates the user interface with new content, based on a routing match for
// the "hash" parameter
Router.prototype.changeTo = function( hash, parameters ) {
    var self = this,
    page = this.getRouteForHash( hash );
 
    // Search for a route that matches the current hash      
    if ( null === page ) {
        page = homePage;
    }
    // If a page is already on display, destroy it
    if ( this.currentPage !== null && typeof( this.currentPage.destroy ) === 'function' ) {
        //window.console.log( 'routing::changeTo', 'destroying current page', this.currentPage );
        this.currentPage.destroy();
    }
   
	this.currentPage = page;

	if(!window.languageInitialized) {
		$.subscribe("LanguageInitilized", function() {
			page.render(parameters);
			page.getPage().languageSubscribe(parameters);
		});
	} else {
		page.render(parameters);
		page.getPage().languageSubscribe(parameters);
	}
	menuView.selectMenu();
};

Router.prototype.routes = [
    {'matcher': new RegExp( /^#home/   ), page: homePage},
    {'matcher': new RegExp( /^#products_milk/   ), page: productsPage},
    {'matcher': new RegExp( /^#products_meat/   ), page: productsMeatPage},
    {'matcher': new RegExp( /^#buy_centers/   ), page: whereToBuyPage},
    {'matcher': new RegExp( /^#history/   ), page: historyPage},
    {'matcher': new RegExp( /^#achievements/   ), page: achievementsPage},
    {'matcher': new RegExp( /^#images_factory/   ), page: imagesCompanyPage},
    {'matcher': new RegExp( /^#images_meetings/   ), page: imagesMeetingsPage},
    {'matcher': new RegExp( /^#videos_products/   ), page: videosPage},
    {'matcher': new RegExp( /^#videos_meetings/   ), page: videosMeetingsPage},
    {'matcher': new RegExp( /^#contactus/   ), page: contactUsPage},
    {'matcher': new RegExp( /^#faq/   ), page: faqPage},
    {'matcher': new RegExp( /^#careers/   ), page: careersPage},
    {'matcher': new RegExp( /^#our_benefits/   ), page: ourBenefitsPage},
     {'matcher': new RegExp( /^#admin/   ), page: adminPage}
];

Router.currentPage = null;

function getUrlHashParameters( hash ) {

    // Hash begins with the #page, then ?, then name=value parameters separated by &
    // e.g. #contact_details?contact_id=1&show_all=true

    // Slice off the page and ?, then split into key/value strings
    var pairStrings = hash.slice( hash.indexOf( '?' ) + 1 ).split( '&' ),
    pairs = {},
    i = 0,
    pairString, pair={},equalSignValidator;

    for ( ; i < pairStrings.length; i++ ) {
        pairString = pairStrings[ i ];
        /*jslint regexp: false*/ equalSignValidator = pairString.replace(/[^=)]/g, "").length;
        if(equalSignValidator > 1){
            pair[0] = pairString.slice(0,pairString.indexOf("="));
            pair[1] = pairString.slice(pairString.indexOf("=")+1);
        }else{
            pair = pairString.split( '=' ); // Split into name/value array
        }
        pairs[ pair[ 0 ] ] = pair[ 1 ];
    }

    return pairs;
};

var router = new Router();