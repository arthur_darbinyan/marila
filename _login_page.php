<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-2" />
		<link rel="shortcut icon" href="img/shortcut-icon.png" type="image/png"/>	
		<title>Maila | Login</title>

		<style type="text/css">
			* {
				margin: 0;
				padding: 0;
			}

			.login_box {
				width: 220px;
				padding: 10px;
				border: 3px solid #CCC;
				font-family: Arial, Helvetica, sans-serif;
				font-size: 10px;
				margin: 100px auto 10px auto;
			}

			.login_title {
				font-size: 12px;
				font-weight: bold;
			}

			.login_text {
				font-size: 18px;
				font-weight: bold;
				color: #999;
			}

			.login_input {
				width: 210px;
				padding: 5px;
				margin: 2px 0 2px 0;
				border: 1px solid #999;
				font-size: 16px;
			}

			.error {
				font-weight: bold;
				color: #C30;
			}
		</style>

		<link href="css/main.css" rel="stylesheet"/>
		<link href="css/css3-styles.css" rel="stylesheet"/>
		<link href="css/css3-styles-ie8.css" rel="stylesheet"/>
	</head>

	<body>

		<div id="page_content" class="content_font">
			<div class="page_content_start_line"></div>
			<div style="padding-top: 30px; float: left;">
				<div style="float: left; width: 450px; height: 420px;">
					<form action="" method="post">

						<span class="login_title">Please Login</span>
						<br />
						You must login to view the following content. Contact admin if you are having problem or have forgotten your password.
						<br />
						<br />
						<?php $login -> error_login(); ?>
						<table width="700" border="0" align="center" cellpadding="2" cellspacing="0">
							<tr>
								<td width="112"><b>Username</b></td>
								<td width="188">
								<input name="username" style="padding: 5px; width: 150px" type="text" id="login" />
								</td>
							</tr>
							<tr>
								<td><b style="margin-top: 10px">Password</b></td>
								<td>
								<input name="password" style="padding: 5px;  width: 150px" type="password" id="password" />
								</td>
							</tr>
							<tr>
								<td></td>
								<td>
								<input id="gobutton" class="red_gradient" style="margin-left: 0px" type="submit" name="login" value="Login" />
								</td>
							</tr>
						</table>

					</form>

				</div>
			</div>

			<div class="" style="width: 250px; float: right; margin-top: 62px;">
				<img src="img/careers/key.png" style="width: 168px" />
			</div>

		</div>
	</body>
</html>