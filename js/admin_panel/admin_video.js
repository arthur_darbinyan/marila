function VideosAdmin() {
	
};

VideosAdmin.initializeView = function(page) {

	var addVideo = $("<input type='button' class='red_gradient admin_add_button' style='margin-top: 20px' value='Add Video' />");
	$("#a_ui").append(addVideo);
	
	addVideo.click(function() {
		VideosAdmin.showAddVideoPopup(page);
	});
	
	$("#video_popup .close_popup").click(function() {
		$('#video_popup').bPopup().close();
	});

};

VideosAdmin.addAdminUI = function(div, videoData, page) {
	var img = $('<img style="float: right; margin-top: 6px; margin-right: 7px; width: 18px;" src="img/delete.png"/>');
	div.append(img);
	img.click(function(event) {
		event.preventDefault();
		var remove = confirm("Delete video?");
		if(remove) {
			VideosAdmin.removeVideo(videoData, page);
		}
	});
};

VideosAdmin.removeVideo = function(videoData, page) {
	
	$.ajax({
		type: "POST",
		url: "admin_panel/actions_controller.php",
		data: {
			action: "RemoveVideo",
			videoData: JSON.stringify(videoData),
			page: page
		}
	}).done(function( msg ) {
		window.location.reload(); 
	}).fail(function(jqXHR, textStatus) {
		alert( "Request failed: " + textStatus );
	});
};

VideosAdmin.showAddVideoPopup = function(page) {
	loadTemplate("templates/admin_panel/AddVideo.html").done(function(html) {

		$("#video_popup_content").html(Mustache.to_html(html, {
            data: {},
            lang: window.lang
        }));
 
        VideosAdmin.setLanguage();
        $('#video_popup').bPopup({});
        
        $("#add_new_video").click(function() {
        	var data = VideosAdmin.validateData();
        	
        	if(data) {
        		VideosAdmin.addVideo(data, page);
        	}
        });
        
        var url = window.URL || window.webkitURL;	
		$("#video_popup .video_img:first").change(function(e) {
			var self = $(this);
		    if( this.disabled ){
		        alert('Your browser does not support File upload.');
		    }else{
		        var chosen = this.files[0];
		        var image = new Image();
		        image.onload = function() {
		        	if(this.width != 405 || this.height != 227) {
		        		alert("Invalid image size - " + 'Width: '+ this.width +'px, Height: '+ this.height + "px\n"
		        		+ "Image size should be Width: 405px, Height: 227px" );
		        		self.val("")
		        	}
		        };
		        image.onerror = function() {
		            alert('Not a valid file type: '+ chosen.type);
		            self.val("");
		        };
		        image.src = url.createObjectURL(chosen);                    
		     }
		});
	});
};

VideosAdmin.addVideo = function(json, page) {
	
	$("#new_video_data").val(JSON.stringify(json));
	$("#new_video_page").val(page);
	$("#add_video_form").submit();
};

VideosAdmin.validateData = function() {
	var json = {
		"arm": {},
		"en": {},
		"ru": {},
	};
	var error = "";
	
	if($("#video_popup .video_img:first").val()=="") {
		error += "Please upload image \n";
	} else if(!isImage($("#video_popup .video_img:first").val())) {
		error += "Invalid File: Please upload png or jpg files\n";
	}
	
	var matcher_ =
    /^http:\/\/(?:www\.)?youtube.com\/watch\?(?=.*v=\w+)(?:\S+)?$/;
    
	if($.trim($("#video_popup .new_video_url:first").val())=="") {
		error += "Please fill video url\n";
	} else if(!$("#video_popup .new_video_url:first").val().match(matcher_)) {
		error += "Invalid youtube url\n";
	} else {
		json.url = $.trim($("#video_popup .new_video_url:first").val());
	}
	
	if($.trim($("#new_video_arm .new_video_title:first").val())=="") {
		error += "Please fill title in Armenian\n";
	} else {
		json.arm.title = $.trim($("#new_video_arm .new_video_title:first").val());
	}
	
	if($.trim($("#new_video_en .new_video_title:first").val())=="") {
		error += "Please fill title in English\n";
	} else {
		json.en.title = $.trim($("#new_video_en .new_video_title:first").val());
	}
	
	if($.trim($("#new_video_ru .new_video_title:first").val())=="") {
		error += "Please fill title in Russian\n";
	} else {
		json.ru.title = $.trim($("#new_video_ru .new_video_title:first").val());
	}
	
	if($.trim(error)!=="") {
		alert(error);
		return false;
	} else {
		return json;
	}
};

VideosAdmin.setLanguage = function() {
	VideosAdmin.currentLanguage = window.currentLanguage;
	$("#admin_language_video ." + VideosAdmin.currentLanguage + ":first").addClass("selected");
	$("#new_video_" + VideosAdmin.currentLanguage).css("display", "block");
	
	$("#admin_language_video").delegate("span", "click", function() {
		
		$("#admin_language_video ." + VideosAdmin.currentLanguage + ":first").removeClass("selected");
		$(this).addClass("selected");
		$("#new_video_" + VideosAdmin.currentLanguage).css("display", "none");
		$("#new_video_" + $(this).attr("data-lang")).css("display", "block");
		VideosAdmin.currentLanguage = $(this).attr("data-lang");
	});
};