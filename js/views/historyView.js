var historyView = new View();
historyView.initialize({
	templateId: "History",
	parentElementId: "page_content",
	templateDependsOnLanguage: true
});

historyView.initilizeView = function(categoryId) {
	
	if(typeof StaticPageAdmin != "undefined" && StaticPageAdmin) {
		StaticPageAdmin.initializeHistoryView();
	}
};

historyView.initilizeListeners = function() {
	this.initilizeView();
};