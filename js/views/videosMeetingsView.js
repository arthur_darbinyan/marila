var videosMeetingsView = new View();
videosMeetingsView.initialize({
	templateId: "VideosMeetings",
	parentElementId: "page_content",
	templateDependsOnLanguage: true
});

videosMeetingsView.initilizeView = function(categoryId) {

    videosMeetingsView.getVideos().done(function(videos) {
    	videosMeetingsView.renderVideos(videos);
    	 if($.support.opacity || videos.length > 2) {
    	 	$("#videos_carousel").touchCarousel({});
    	 }
   		 $("#videos_carousel .image_reflect").reflect({height: 0.2, opacity: 0.3});
    });
    
    if(typeof VideosAdmin!="undefined" && VideosAdmin) {
		VideosAdmin.initializeView("meetings");
	}
};

videosMeetingsView.renderVideos = function(videos) {
	$.each(videos, function(index, video) {
		var left = 40;
		if(index==0) {
			left = 55;
		}
		var videoHtml = $('<li class="touchcarousel-item" style="margin-left: ' + left + 'px; margin-top: 50px;">'
		+ '<a href="' + "javascript:void jQuery.colorbox({html:'<iframe width=640 height=360 "
		+ "src= " + video.url + "  frameborder=0 allowfullscreen></iframe>',"
		+ "transition: 'elastic', opacity: 0.6})" + '"' + ">"
		+ '<div><div class="video_top"><span>' +video["title-" + Localization.currentLanguage()] + '</span></div>'
		+ '<img src="img/play_video.png" style="position: absolute; margin: 97px 182px"/>'
		+ '<img src="' + video.image + '" class="image_reflect" /></div></a></li>');
		
		$("#videos_carousel .touchcarousel-container:first").append(videoHtml);
		
		if(typeof VideosAdmin!="undefined" && VideosAdmin) {
			VideosAdmin.addAdminUI(videoHtml.find(".video_top:first"), video, "meetings");
		}
	});
	$("#videos_carousel .touchcarousel-container:first").append($('<li class="touchcarousel-item" style="width: 55px;"></li>'));
};

videosMeetingsView.getVideos = function() {
	var videos = [];
	var deferred = $.Deferred();
	$.ajaxSetup({
		cache: false
	});
	
	$.get('data/videosPage/data-meetings.xml', function(data) {
		$(data).find('video').each(function() {
			
			var $video = $(this);
			videos.push({
				id: $video.attr("id"),
				url: $video.attr("url"),
				image: $video.attr("image"),
				'title-en': $video.attr("title-en"),
				'title-ru': $video.attr("title-ru"),
				'title-arm': $video.attr("title-arm")
			});
		});
		deferred.resolve(videos.reverse());
	});
	return deferred;
};

videosMeetingsView.initilizeListeners = function() {
	this.initilizeView();
};