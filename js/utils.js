function loadTemplate(url) {
	var deferred = $.Deferred();
	
	// Load and extract the template
	$.ajax({
		url : url,
		success : function(data) {
			deferred.resolve(data);
		},
		dataType : "html",
		cache : false
	});

	return deferred;
};

function isImage(str) {
	str = str.toLowerCase();
	return str.match(/png$/) || str.match(/jpg$/) || str.match(/jpeg$/); 
};

var sliderItems;
function loadSliderItems() {
	var deferred = $.Deferred();
	if(sliderItems) {
		deferred.resolve(sliderItems);
	} else {
		var items = [];
		$.ajaxSetup({ cache: false });
		$.get('data/slider/content-' + Localization.currentLanguage() + '.xml', function(data) {
			$(data).find('item').each(function() {
				var $item = $(this);
				items.push({
					id: $item.attr("id"),
					index: $item.attr("index"),
					href: $item.attr("href"),
					src: $item.attr("src"),
					title: $item.find("title:first").text(),
					text: $item.find("text:first").text()
				});
			});
			sliderItems = items;
	
			sliderItems.sort(function(item1, item2) {
				return item1.index > item2.index;
			});
			deferred.resolve(sliderItems);
			$.ajaxSetup({ cache: true });
		});
		$.subscribe("LanguageChanged", function() { sliderItems = null; });
	}
	return deferred;
};


function loadMapData(xml) {
	var map = {
		markers: []
	};
	
	var deferred = $.Deferred();
	$.ajaxSetup({
		cache: false
	});
	
	$.get(xml, function(data) {
		
		map.zoom = $(data).find('zoom:first').attr("val");
		var _center = $(data).find('center:first');
		map.center = {
			lat: _center.attr("lat"),
			lng: _center.attr("lng")
		};

		$(data).find('marker').each(function() {
			var $marker = $(this);
			map.markers.push({
				lat: $marker.attr("lat"),
				lng: $marker.attr("lng")
			});
		});
		deferred.resolve(map);
	});
	return deferred;
};
