function Application() {
	
	this.initialize = function() {
		initializeListeners();
		loadStyleFiles();
	};
	
	function loadStyleFiles() {
		if (!$.support.opacity) {
			loadJsCssfile("css/css3-styles-ie8.css");
		} else {
			loadJsCssfile("css/css3-styles.css");
		}
	};

	function loadJsCssfile(filename) {
		if (!$.support.opacity) {
			var fileref = document.createElement("link");
			fileref.setAttribute("rel", "stylesheet");
			fileref.setAttribute("type", "text/css");
			fileref.setAttribute("href", filename);
			if ( typeof fileref != "undefined") {
				document.getElementsByTagName("head")[0].appendChild(fileref);
			}
		} else {
			$('head').append($('<link rel="stylesheet" type="text/css" />').attr('href', filename));
		}
	};
	
	function initializeListeners() {
		
		$(window).ready(function() {
			$("#logo").css("left", (parseInt($('body').innerWidth() - 960)) / 2   + "px");
		});
		
		$(window).resize(function() {
			if((parseInt($('body').innerWidth() - 960) > 0)) {
				$("#logo").css("left", (parseInt($('body').innerWidth() - 960)) / 2   + "px");
			}
		});
	};
};

var app = new Application();
app.initialize();