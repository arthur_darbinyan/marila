<?php

	require_once('SimpleImage.php');
	if(file_exists('check_login.php')) {
		require_once('check_login.php');
	} else {
		require_once('../check_login.php');
	}
	require_once('simple_html_dom.php');
	
	class MarilaApi {
		
		
		public function saveQuestion($language, $data) {
			
			if(!isLoggedIn()) {
				return 'Not Admin User';
			}
			
			$filename = "../data/faqPage/content-" . $language . ".xml";
			
			$data = get_magic_quotes_gpc() ? stripslashes($data) : $data;
			$dataJSON = json_decode($data);
			
			$xsl = new DOMDocument;
			$xsl->load($filename);
			$xpath = new DomXPath($xsl);
			
			foreach($xpath->query("//question[@id='".$dataJSON->id."']") as $node) {
				$node->getElementsByTagName("title")->item(0)->nodeValue = $dataJSON->title;
				$node->getElementsByTagName("text")->item(0)->nodeValue = $dataJSON->text;
			}
			
			$xsl->save($filename);
			return "Question successfully updated!";
		}
		
		public function addQuestionAllLangs($data) {
			
			if(!isLoggedIn()) {
				return 'Not Admin User';
			}
			
			$armFilename = "../data/faqPage/content-arm.xml";
			$ruFilename = "../data/faqPage/content-ru.xml";
			$enFilename = "../data/faqPage/content-en.xml";
			
			$questionID = md5(uniqid(rand(), true));
			$data = get_magic_quotes_gpc() ? stripslashes($data) : $data;
			$dataJSON = json_decode($data);
			$this -> addQuestion($armFilename, $dataJSON->arm, $questionID);
			$this -> addQuestion($ruFilename, $dataJSON->ru, $questionID);
			$this -> addQuestion($enFilename, $dataJSON->en, $questionID);
			return "Question successfully added!";
		}
		
		private function addQuestion($xmlFilename, $questionData, $questionID) {
			
			if(!isLoggedIn()) {
				return 'Not Admin User';
			}
			$xsl = new DOMDocument;
			$xsl->load($xmlFilename);
			$xpath = new DomXPath($xsl);
			
			$questionElement = $xsl->createElement("question");
			
			$idAttribute = $xsl->createAttribute("id");
			$idAttribute->value = $questionID;
			
			$questionElement->appendChild($idAttribute);
		
			$titleElement = $xsl->createElement("title");
			$titleText = $xsl->createTextNode($questionData->title);
			$titleElement->appendChild($titleText);
			
			$textElement = $xsl->createElement("text");
			$textText = $xsl->createTextNode($questionData->text);
			$textElement->appendChild($textText);
			
			$questionElement->appendChild($titleElement);
			$questionElement->appendChild($textElement);
			
			$root = $xsl->documentElement;
			$root->appendChild($questionElement);
			
			$xsl->save($xmlFilename);

		}
		
		public function removeQuestionAllLangs($questionId) {
			if(!isLoggedIn()) {
				return 'Not Admin User';
			}

			$armFilename = "../data/faqPage/content-arm.xml";
			$enFilename = "../data/faqPage/content-en.xml";
			$ruFilename = "../data/faqPage/content-ru.xml";
			
			$this -> removeQuestion($armFilename, $questionId);
			$this -> removeQuestion($ruFilename, $questionId);
			$this -> removeQuestion($enFilename, $questionId);
			return "Question successfully removed!";
		}
		
		private function removeQuestion($file, $questionId) {
					
			if(!isLoggedIn()) {
				return 'Not Admin User';
			}
			$xsl = new DOMDocument;
			$xsl->load($file); 
			$xpath = new DomXPath($xsl);

			foreach($xpath->query("//question[@id='".$questionId."']") as $node) {
				$node->parentNode->removeChild($node);
			}

			$xsl->save($file);
		}
		

		public function saveMap($lat, $lng, $zoom, $page) {
			if(!isLoggedIn()) {
				return 'Not Admin User';
			}
			$xsl = new DOMDocument;
			$xsl->preserveWhiteSpace = false;
			
			switch($page) {
				case "where_to_buy":
					$filename = "../data/whereToBuyPage/map.xml";
					break;
				case "contact_us":
					$filename = "../data/contactUsPage/map.xml";
					break;
			}
			
			$xsl->load($filename); 
			$xpath = new DomXPath($xsl);
	
			foreach($xpath->query("//zoom") as $node) {
				$node->setAttribute("val", $zoom);
			}
			
			foreach($xpath->query("//center") as $node) {
				$node->setAttribute("lat", $lat);
				$node->setAttribute("lng", $lng);
			}
		
			$xsl->save($filename);
			return "Market successfully saved!";
		}
		
		public function removeMarker($lat, $lng, $page) {
			if(!isLoggedIn()) {
				return 'Not Admin User';
			}
			$xsl = new DOMDocument;
			$xsl->preserveWhiteSpace = false;
			
			switch($page) {
				case "where_to_buy":
					$filename = "../data/whereToBuyPage/map.xml";
					break;
				case "contact_us":
					$filename = "../data/contactUsPage/map.xml";
					break;
			}
			
			$xsl->load($filename); 
			$xpath = new DomXPath($xsl);
	
			foreach($xpath->query("//marker[@lng='".$lng."']") as $node) {
				if($node->getAttribute("lat") == $lat) {
					$node->parentNode->removeChild($node);
				}
			}
		
			$xsl->save($filename);
			return "Market successfully removed!";
		}
		
		public function addMarker($lat, $lng, $page) {
			if(!isLoggedIn()) {
				return 'Not Admin User';
			}
			$xsl = new DOMDocument;
			$xsl->preserveWhiteSpace = false;
			
			switch($page) {
				case "where_to_buy":
					$filename = "../data/whereToBuyPage/map.xml";
					break;
				case "contact_us":
					$filename = "../data/contactUsPage/map.xml";
					break;
			}
			
			$xsl->load($filename); 
			$xpath = new DomXPath($xsl);
			
			$markerElement = $xsl->createElement("marker");
			
			$lngAttribute = $xsl->createAttribute("lng");
			$lngAttribute->value = $lng;
			
			$latAttribute = $xsl->createAttribute("lat");
			$latAttribute->value = $lat;
			
			$markerElement->appendChild($latAttribute);
			$markerElement->appendChild($lngAttribute);
			
			$root = $xsl->documentElement;
			$root->appendChild($markerElement);
		
			$xsl->save($filename);
			return "Market successfully added!";
		}
		
		public function removeVideo($video_data, $page) {
			if(!isLoggedIn()) {
				return 'Not Admin User';
			}
			
			$video_data = get_magic_quotes_gpc() ? stripslashes($video_data) : $video_data;
			$dataJSON = json_decode($video_data);
			$xsl = new DOMDocument;
			$xsl->preserveWhiteSpace = false; 
			if($page == "products") {
				$filename = "../data/videosPage/data-products.xml";
			} else if($page == "meetings") {
				$filename = "../data/videosPage/data-meetings.xml";
			}
			$xsl->load($filename); 
			$xpath = new DomXPath($xsl);
			$response;
			foreach($xpath->query("//video[@id='".$dataJSON->id."']") as $node) {
				$node->parentNode->removeChild($node);
				unlink('../'.$node->getAttribute("image"));
			}
		
			$xsl->save($filename);
			return true;
		}
		
		public function addVideo($video_data, $page) {
					
			if(!isLoggedIn()) {
				return 'Not Admin User';
			}
			
			$productImage = upload("video_img", 'img/video_imgs/');
			
			$xsl = new DOMDocument;
			$xsl->preserveWhiteSpace = false;
			if($page == "products") {
				$filename = "data/videosPage/data-products.xml";
			} else if($page == "meetings") {
				$filename = "data/videosPage/data-meetings.xml";
			}
			$xsl->load($filename); 
			$xpath = new DomXPath($xsl);
			
			$video_data = get_magic_quotes_gpc() ? stripslashes($video_data) : $video_data;
			$dataJSON = json_decode($video_data);
			$videoElement = $xsl->createElement("video");
			
			$idAttribute = $xsl->createAttribute("id");
			$idAttribute->value = md5(uniqid(rand(), true));
			
			// get youtube video id
			parse_str( parse_url( $dataJSON->url, PHP_URL_QUERY ), $my_array_of_vars );
			
			$urlAttribute = $xsl->createAttribute("url");
			$urlAttribute->value = "http://www.youtube.com/embed/".$my_array_of_vars['v'];
			
			$image = new SimpleImage();
			$image->load($productImage);
			$image->resizeToWidth(405); 
			$image->save($productImage);
			
			$imageAttribute = $xsl->createAttribute("image");
			$imageAttribute->value = $productImage;
			
			$titleEn = $xsl->createAttribute("title-en");
			$titleEn->value = $dataJSON->en->title;
			$titleRu = $xsl->createAttribute("title-ru");
			$titleRu->value = $dataJSON->ru->title;
			$titleArm = $xsl->createAttribute("title-arm");
			$titleArm->value = $dataJSON->arm->title;
			
			$videoElement->appendChild($idAttribute);
			$videoElement->appendChild($urlAttribute);
			$videoElement->appendChild($imageAttribute);
			$videoElement->appendChild($titleEn);
			$videoElement->appendChild($titleRu);
			$videoElement->appendChild($titleArm);
			
			$root = $xsl->documentElement;
			$root->appendChild($videoElement);
			
			$xsl->save($filename);
			return true;
		}
		
		public function updateStaticPageImage($language, $page, $img) {
		
			if(!isLoggedIn()) {
				return 'Not Admin User';
			}
			
			$filenameArm = $filenameEn = $filenameRu = "templates/";
			switch($page) {
				case "history_page":
					$filenameArm .= "History-";
					$filenameEn .= "History-";
					$filenameRu .= "History-";
					break;
				case "achievements_page":
					$filenameArm .= "Achievements-";
					$filenameEn .= "Achievements-";
					$filenameRu .= "Achievements-";
					break;
				case "careers_page":
					$filenameArm .= "Careers-";
					$filenameEn .= "Careers-";
					$filenameRu .= "Careers-";
					break;
				case "benifits_page":
					$filenameArm .= "OurBenefits-";
					$filenameEn .= "OurBenefits-";
					$filenameRu .= "OurBenefits-";
					break;
			}
			$filenameArm .= "arm.html";
			$filenameEn .= "en.html";
			$filenameRu .= "ru.html";
			
			$sliderImage = upload($img, 'img/spage_images/');
			
			$image = new SimpleImage();
			$image->load($sliderImage);
			$image->resizeToWidth(500); 
			$image->save($sliderImage);
			
			$htmlArm = file_get_html($filenameArm);
			if(file_exists($htmlArm->find('img', 0)->src)) {
				unlink($htmlArm->find('img', 0)->src);
			}
			$htmlArm->find('img', 0)->src = $sliderImage;
			
			$file = fopen($filenameArm, 'w') or die("can't open file");
			fwrite($file, $htmlArm);
			fclose($file);
			
			$htmlRu = file_get_html($filenameRu);
			if(file_exists($htmlRu->find('img', 0)->src)) {
				unlink($htmlRu->find('img', 0)->src);
			}
			$htmlRu->find('img', 0)->src = $sliderImage;
			
			$file = fopen($filenameRu, 'w') or die("can't open file");
			fwrite($file, $htmlRu);
			fclose($file);
			
			$htmlEn = file_get_html($filenameEn);
			if(file_exists($htmlEn->find('img', 0)->src)) {
				unlink($htmlEn->find('img', 0)->src);
			}
			$htmlEn->find('img', 0)->src = $sliderImage;
			
			$file = fopen($filenameEn, 'w') or die("can't open file");
			fwrite($file, $htmlEn);
			fclose($file);
			
			//return $htmlEn;
			return "Succesfully saved!";
		}
		
		public function updateStaticPageText($language, $page, $text) {
					
			if(!isLoggedIn()) {
				return 'Not Admin User';
			}
			
			$filename = "../templates/";
			switch($page) {
				case "history_page":
					$filename .= "History-";
					break;
				case "achievements_page":
					$filename .= "Achievements-";
					break;
				case "careers_page":
					$filename .= "Careers-";
					break;
				case "benifits_page":
					$filename .= "OurBenefits-";
					break;
				case "where_to_buy":
					$filename .= "WhereToBuy-";
					break;
				case "contact_us":
					$filename .= "ContactUs-";
					break;
			}
			$filename .= $language.".html";
			
			$text = get_magic_quotes_gpc() ? stripslashes($text) : $text;
			$html = file_get_html($filename);
			$html->find('div[id=text_content]', 0)->innertext = $text;
			
			$file = fopen($filename, 'w') or die("can't open file");
			fwrite($file, $html);
			fclose($file);
			
			return "Succesfully saved!";
		}
		
		public function editSlider($data) {
						
			if(!isLoggedIn()) {
				return 'Not Admin User';
			}
			
			$data = get_magic_quotes_gpc() ? stripslashes($data) : $data;
			$dataJSON = json_decode($data);
			
			$armFilename = "../data/slider/content-arm.xml";
			$ruFilename = "../data/slider/content-ru.xml";
			$enFilename = "../data/slider/content-en.xml";
			
			$this -> saveSliderItem($armFilename, $dataJSON->arm, $dataJSON->id, $dataJSON->href);
			$this -> saveSliderItem($ruFilename, $dataJSON->ru, $dataJSON->id, $dataJSON->href);
			$this -> saveSliderItem($enFilename, $dataJSON->en, $dataJSON->id, $dataJSON->href);
		}
		
		private function saveSliderItem($filename, $sliderData, $id, $pageRef) {
					
			if(!isLoggedIn()) {
				return 'Not Admin User';
			}
			$xsl = new DOMDocument;
			$xsl->load($filename); 
			$xpath = new DomXPath($xsl);
			
			foreach($xpath->query("//item[@id='".$id."']") as $node) {
				$node->getElementsByTagName("title")->item(0)->nodeValue = $sliderData->title;
				$node->getElementsByTagName("text")->item(0)->nodeValue = $sliderData->text;
				$node->setAttribute("href", $pageRef);
			}
			$xsl->save($filename);
		}
		
		public function addSlider($img, $data) {
					
			if(!isLoggedIn()) {
				return 'Not Admin User';
			}
			$sliderImage = upload($img, 'img/slider/');
			
			$armFilename = "data/slider/content-arm.xml";
			$ruFilename = "data/slider/content-ru.xml";
			$enFilename = "data/slider/content-en.xml";
			
			$sliderItemID = md5(uniqid(rand(), true));
			$data = get_magic_quotes_gpc() ? stripslashes($data) : $data;
			$dataJSON = json_decode($data);
			
			$this -> addSliderItem($armFilename, $dataJSON->arm, $dataJSON->index, $dataJSON->href, $sliderImage, $sliderItemID);
			$this -> addSliderItem($ruFilename, $dataJSON->ru, $dataJSON->index, $dataJSON->href, $sliderImage, $sliderItemID);
			$this -> addSliderItem($enFilename, $dataJSON->en, $dataJSON->index, $dataJSON->href, $sliderImage, $sliderItemID);
		}
		
		private function addSliderItem($xmlFilename, $sliderData, $index, $pageRef, $sliderImageURL, $sliderItemID) {
					
			if(!isLoggedIn()) {
				return 'Not Admin User';
			}
			$xsl = new DOMDocument;
			$xsl->load($xmlFilename);
			$xpath = new DomXPath($xsl);
			
			foreach($xpath->query("//item[@index>='".$index."']") as $node) {
				$node->setAttribute("index", strval(intval($node->getAttribute("index")) + 1));
			}
			
			$sliderItemElement = $xsl->createElement("item");
			
			$idAttribute = $xsl->createAttribute("id");
			$idAttribute->value = $sliderItemID;
			
			$indexAttribute = $xsl->createAttribute("index");
			$indexAttribute->value = $index;
			
			$hrefAttribute = $xsl->createAttribute("href");
			$hrefAttribute->value = $pageRef;

			$srcAttribute = $xsl->createAttribute("src");
			$srcAttribute->value = $sliderImageURL;
			
			$sliderItemElement->appendChild($idAttribute);
			$sliderItemElement->appendChild($indexAttribute);
			$sliderItemElement->appendChild($hrefAttribute);
			$sliderItemElement->appendChild($srcAttribute);

			$titleElement = $xsl->createElement("title");
			$titleText = $xsl->createTextNode($sliderData->title);
			$titleElement->appendChild($titleText);
			
			$textElement = $xsl->createElement("text");
			$textText = $xsl->createTextNode($sliderData->text);
			$textElement->appendChild($textText);
			
			$sliderItemElement->appendChild($titleElement);
			$sliderItemElement->appendChild($textElement);
			
			$root = $xsl->documentElement;
			$root->appendChild($sliderItemElement);
			
			$xsl->save($xmlFilename);
		}
		
		public function removeSliderItem($id, $index, $src) {
			if(!isLoggedIn()) {
				return 'Not Admin User';
			}
			
			unlink('../'.$src);
			$xsl = new DOMDocument;
			$xsl->preserveWhiteSpace = false; 
		
			$armFilename = "../data/slider/content-arm.xml";
			$ruFilename = "../data/slider/content-ru.xml";
			$enFilename = "../data/slider/content-en.xml";
			
			$this -> removeSliderImage($armFilename, $id, $index);
			$this -> removeSliderImage($ruFilename, $id, $index);
			$this -> removeSliderImage($enFilename, $id, $index);
			return $id;
		}
		
		private function removeSliderImage($file, $id, $index) {
					
			if(!isLoggedIn()) {
				return 'Not Admin User';
			}
			$xsl = new DOMDocument;
			$xsl->load($file); 
			$xpath = new DomXPath($xsl);
			
			foreach($xpath->query("//item[@index>'".$index."']") as $node) {
				$node->setAttribute("index", strval(intval($node->getAttribute("index")) - 1));
			}

			foreach($xpath->query("//item[@id='".$id."']") as $node) {
				$node->parentNode->removeChild($node);
			}

			
			$xsl->save($file);
		}
		
		public function removeImage($imgId, $page) {
			if(!isLoggedIn()) {
				return 'Not Admin User';
			}
			$xsl = new DOMDocument;
			$xsl->preserveWhiteSpace = false; 
			if($page == "company") {
				$filename = "../data/imagesPage/our_company/data.xml";
			} else if($page == "meetings") {
				$filename = "../data/imagesPage/meetings/data.xml";
			}
			$xsl->load($filename); 
			$xpath = new DomXPath($xsl);
			$response;
			foreach($xpath->query("//img[@id='".$imgId."']") as $node) {
				$node->parentNode->removeChild($node);
				unlink('../'.$node->getAttribute("smallURL"));
				unlink('../'.$node->getAttribute("bigURL"));
				$response = $node->getAttribute("smallURL");
			}
		
			$xsl->save($filename);
			return true;
		}
		
		public function addImages($images, $page) {
			if(!isLoggedIn()) {
				return 'Not Admin User';
			}
			$addedImages = array();
			foreach ($images as $i => $value) {
			   $addedImages[] = upload($value, 'img/images/');
			}
			
			$xsl = new DOMDocument;
			$xsl->preserveWhiteSpace = false; 
			if($page == "company") {
				$filename = "data/imagesPage/our_company/data.xml";
			} else if($page == "meetings") {
				$filename = "data/imagesPage/meetings/data.xml";
			}
			$xsl->load($filename); 
			$xpath = new DomXPath($xsl);
		
			$imgElement = $xsl->createElement("img");
			
			$idAttribute = $xsl->createAttribute("id");
			$idAttribute->value = md5(uniqid(rand(), true));
			
			$smallImgAttribute = $xsl->createAttribute("smallURL");
			$smallImgAttribute->value = $addedImages[0];
			
			$bigImgAttribute = $xsl->createAttribute("bigURL");
			$bigImgAttribute->value = $addedImages[1];
			
			$imgElement->appendChild($idAttribute);
			$imgElement->appendChild($smallImgAttribute);
			$imgElement->appendChild($bigImgAttribute);
			
			$root = $xsl->documentElement;
			$root->appendChild($imgElement);
			
			$xsl->save($filename);
			return true;
		}

		public function addProduct($description, $productCategory, $productType) {
			
			if(!isLoggedIn()) {
				return 'Not Admin User';
			}
			$productImage = upload("product_image", 'img/products/');
			
			$ext = pathinfo($_FILES['product_image']['name'], PATHINFO_EXTENSION);
			$withoutExt = preg_replace("/\\.[^.\\s]{3,4}$/", "", $productImage);
			
			$smallImage = $withoutExt.'_small.'.$ext;
			$image = new SimpleImage();
			$image->load($productImage);
			if($image->getWidth() >= $image->getHeight()) {
				$image->resizeToWidth(180); 
			} else {
				$image->resizeToHeight(180); 
			}
			$image->savePNG($smallImage);
			
			$xsl = new DOMDocument;
		//	$xsl->preserveWhiteSpace = false;
			$filename = "";
			if($productType=="milk") {
				$filename = "data/milkProductsPage/data.xml";
			} else {
				$filename = "data/meatProductsPage/data.xml";
			}
			
			$xsl->load($filename); 
			$xpath = new DomXPath($xsl);
			
			$productElement = $xsl->createElement("product");
			$idAttribute = $xsl->createAttribute("id");
			$idAttribute->value = md5(uniqid(rand(), true));
			
			$iconAttribute = $xsl->createAttribute("data-icon");
			$iconAttribute->value = $smallImage;
			
			$iconWidthAttribute = $xsl->createAttribute("data-icon-width");
			//$iconWidthAttribute->value = 120;
			
			$description = get_magic_quotes_gpc() ? stripslashes($description) : $description;
			
			$descriptionJSON = json_decode($description);
			$titleEn = $xsl->createAttribute("title-en");
			$titleEn->value = $descriptionJSON->en->title;
			$titleRu = $xsl->createAttribute("title-ru");
			$titleRu->value = $descriptionJSON->ru->title;
			$titleArm = $xsl->createAttribute("title-arm");
			$titleArm->value = $descriptionJSON->arm->title;
			
			$dataDetailsAttribute = $xsl->createAttribute("data-details");
			if($productType=="milk") {
				$dataDetailsAttribute->value = "data/milkProductsPage/product-" . $idAttribute->value . ".json";
			} else {
				$dataDetailsAttribute->value = "data/meatProductsPage/product-" . $idAttribute->value . ".json";
			}
			// Write the contents back to the file
			
			$descriptionJSON->image = $productImage;
			$fileName = $dataDetailsAttribute->value;
			$file = fopen($fileName, 'w') or die("can't open file");
			fwrite($file, json_encode($descriptionJSON));
			fclose($file);
			
			$productElement->appendChild($idAttribute);
			$productElement->appendChild($iconAttribute);
			$productElement->appendChild($iconWidthAttribute);
			$productElement->appendChild($dataDetailsAttribute);
			$productElement->appendChild($titleEn);
			$productElement->appendChild($titleRu);
			$productElement->appendChild($titleArm);
			
			foreach($xpath->query("//productsType[@type-id='".$productCategory."']") as $node) {
				$node->appendChild($productElement);
			}
			
			$xsl->save($filename);
			return true;
		}
		
		public function editProduct($productType, $description, $productInfo, $imageName) {
			
			if(!isLoggedIn()) {
				return 'Not Admin User';
			}

			$productPage = "";
			if($productType == "milk") {
				$productPage = "milkProductsPage";
			} else {
				$productPage = "meatProductsPage";
			}

			if($imageName) {
				$newImage = upload($imageName, 'img/products/');
				
				$ext = pathinfo($_FILES[$imageName]['name'], PATHINFO_EXTENSION);
				$withoutExt = preg_replace("/\\.[^.\\s]{3,4}$/", "", $newImage);
				
				$smallImage = $withoutExt.'_small.'.$ext;
				$image = new SimpleImage();
				$image->load($newImage);
				if($image->getWidth() >= $image->getHeight()) {
					$image->resizeToWidth(180); 
				} else {
					$image->resizeToHeight(180); 
				}
				$image->savePNG($smallImage);
			}		
			$xsl = new DOMDocument;
			// $xsl->preserveWhiteSpace = false;
		
			$filename = "data/".$productPage."/data.xml";
			$xsl->load($filename); 
			$xpath = new DomXPath($xsl);
			
			$description = get_magic_quotes_gpc() ? stripslashes($description) : $description;
			$productInfo = get_magic_quotes_gpc() ? stripslashes($productInfo) : $productInfo;
			
			$descriptionJSON = json_decode($description);
			$productInfoJSON = json_decode($productInfo);
			
			foreach($xpath->query("//product[@id='".$productInfoJSON->productId."']") as $node) {
				$node->setAttribute("title-en", $descriptionJSON->en->title);
				$node->setAttribute("title-ru", $descriptionJSON->ru->title);
				$node->setAttribute("title-arm", $descriptionJSON->arm->title);
				if(isset($smallImage)) {
					$node->setAttribute("data-icon", $smallImage);
				}
			}
			$xsl->save($filename);
			
			$dataDetailsAttribute = $xsl->createAttribute("data-details");
			$dataDetailsAttribute->value = "data/".$productPage."/product-" . $productInfoJSON->productId . ".json";
			if(isset($newImage)) {
				$descriptionJSON->image = $newImage;
			}
			
			// Write the contents back to the file
			$fileName = $dataDetailsAttribute->value;
			$file = fopen($fileName, 'w') or die("can't open file");
			fwrite($file, json_encode($descriptionJSON));
			fclose($file);
		}

		public function deleteProduct($productId, $productType) {
		
			if(!isLoggedIn()) {
				return 'Not Admin User';
			}
			$productPage = "";
			if($productType == "milk") {
				$productPage = "milkProductsPage";
			} else {
				$productPage = "meatProductsPage";
			}
			$filename = "../data/".$productPage."/product-" . $productId . ".json";
			unlink($filename);
			
			$xsl = new DOMDocument;
			// $xsl->preserveWhiteSpace = false;
		
			$filename = "../data/".$productPage."/data.xml";
			$xsl->load($filename); 
			$xpath = new DomXPath($xsl);
			
			foreach($xpath->query("//product[@id='".$productId."']") as $node) {
				$node->parentNode->removeChild($node);
			}
			$xsl->save($filename);
			return $productId;
		}
		
		public function deleteVacancy($vacancyId) {
		
			if(!isLoggedIn()) {
				return 'Not Admin User';
			}
			$filename = "../data/careersPage/vacancy-" . $vacancyId . ".json";
			unlink($filename);
			$xsl = new DOMDocument;
			// $xsl->preserveWhiteSpace = false;
		
			$filename = "../data/careersPage/data.xml";
			$xsl->load($filename); 
			$xpath = new DomXPath($xsl);
			
			foreach($xpath->query("//career[@id='".$vacancyId."']") as $node) {
				$node->parentNode->removeChild($node);
			}
			$xsl->save($filename);
			return $vacancyId;
		}
		
		public function addVacancy($data) {	
			if(!isLoggedIn()) {
				return 'Not Admin User';
			}
			$xsl = new DOMDocument;
			$filename = "../data/careersPage/data.xml";
			$xsl->load($filename);
			$xpath = new DomXPath($xsl);
			
			$careerElement = $xsl->createElement("career");
			$idAttribute = $xsl->createAttribute("id");
			$idAttribute->value = md5(uniqid(rand(), true));
			
			$data = get_magic_quotes_gpc() ? stripslashes($data) : $data;
			$dataJSON = json_decode($data);
			$titleEn = $xsl->createAttribute("title-en");
			$titleEn->value = $dataJSON->en->title;
			$titleRu = $xsl->createAttribute("title-ru");
			$titleRu->value = $dataJSON->ru->title;
			$titleArm = $xsl->createAttribute("title-arm");
			$titleArm->value = $dataJSON->arm->title;

			$careerElement->appendChild($idAttribute);
			$careerElement->appendChild($titleEn);
			$careerElement->appendChild($titleRu);
			$careerElement->appendChild($titleArm);
			
			$root = $xsl->documentElement;
			$root->appendChild($careerElement);
			
			$xsl->save($filename);
			
			// Write the contents back to the file
			$fileName = "../data/careersPage/vacancy-" . $idAttribute->value . ".json";
			$file = fopen($fileName, 'w') or die("can't open file");
			fwrite($file, $data);
			fclose($file);
			
			return $idAttribute->value ;
		}
	}
?> 