var ourBenefitsView = new View();
ourBenefitsView.initialize({
	templateId: "OurBenefits",
	parentElementId: "page_content",
	templateDependsOnLanguage: true
});

ourBenefitsView.initilizeView = function(categoryId) {
	if(typeof StaticPageAdmin != "undefined" && StaticPageAdmin) {
		StaticPageAdmin.initializeBenefitsView();
	}
};

ourBenefitsView.initilizeListeners = function() {
	this.initilizeView();
};