var CookieStorage = function() {
	// Get an object that holds all cookies
	var cookies = ( function() {
			var cookies = {}, all = document.cookie, list, i = 0, cookie, firstEq, name, value;
			if (all === "") {
				return cookies;
			}

			list = all.split("; ");
			// Split into individual name=value pairs

			for (; i < list.length; i += 1) {
				cookie = list[i];
				firstEq = cookie.indexOf("=");
				// Find the first = sign
				name = cookie.substring(0, firstEq);
				// Get cookie name
				value = cookie.substring(firstEq + 1);
				// Get cookie value
				value = decodeURIComponent(value);
				// Decode the value

				cookies[name] = value;
			}
			return cookies;
		}()),

	// Collect the cookie names in an array
	keys = [], key;
	for (key in cookies) {
		if (cookies.hasOwnProperty(key)) {
			keys.push(key);
		}

	}

	// Public API
	this.length = keys.length;

	// Return the name of the nth cookie, or null if n is out of range
	this.key = function(n) {
		if (n < 0 || n >= keys.length) {
			return null;
		}

		return keys[n];
	};

	// Return the value of the named cookie, or null.
	this.getItem = function(name) {
		if (arguments.length !== 1) {
			throw new Error("Provide one argument");
		}

		return cookies[name] || null;
	};

	this.setItem = function(key, value) {
		if (arguments.length !== 2) {
			throw new Error("Provide two arguments");
		}

		if (cookies[key] === undefined) {// If no existing cookie with this name
			keys.push(key);
			this.length++;
		}

		cookies[key] = value;

		var cookie = key + "=" + encodeURIComponent(value), today = new Date(),
		expiry = new Date(today.getTime() + 30 * 24 * 3600 * 1000);
		// Add cookie attributes to that string

		cookie += "; max-age=" + expiry;

		cookie += "; path=/";

		// Set the cookie through the document.cookie property
		document.cookie = cookie;
	};

	// Remove the specified cookie
	this.removeItem = function(key) {
		if (arguments.length !== 1) {
			throw new Error("Provide one argument");
		}

		var i = 0, max;
		if (cookies[key] === undefined) {// If it doesn't exist, do nothing
			return;
		}

		// Delete the cookie from our internal set of cookies
		delete cookies[key];

		// And remove the key from the array of names, too.
		for ( max = keys.length; i < max; i += 1) {
			if (keys[i] === key) {// When we find the one we want
				keys.splice(i, 1);
				// Remove it from the array.
				break;
			}
		}
		this.length--;
		// Decrement cookie length

		// Actually delete the cookie
		document.cookie = key + "=; max-age=0";
	};

	// Remove all cookies
	this.clear = function() {
		var i = 0;
		for (; i < keys.length; i++) {
			document.cookie = keys[i] + "=; max-age=0";
		}

		// Reset our internal state
		cookies = {};
		keys = [];
		this.length = 0;
	};
}; 