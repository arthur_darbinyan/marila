<?php
function upload($file_id, $folder = "") {
	if ($_FILES[$file_id]['name']) {
		//if no errors...
		if (!$_FILES[$file_id]['error']) {
			//now is the time to modify the future file name and validate the file
			$new_file_name = uniqid(rand(), true);
			$ext = pathinfo($_FILES[$file_id]['name'], PATHINFO_EXTENSION);
			//rename file
			$valid_file = true;
			if ($_FILES[$file_id]['size'] > (10*1024000))//can't be larger than 10 MB
			{
				$valid_file = false;
				$message = 'Oops!  Your file\'s size is to large.';
				return false;
			}

			//if the file has passed the test
			if ($valid_file) {
				//move it to where we want it to be
				move_uploaded_file($_FILES[$file_id]['tmp_name'], $folder . $new_file_name. "." . $ext);
				return $folder . $new_file_name. "." . $ext;
			}
		}
		//if there is an error...
		else {
			//set that to be the returned message
			$message = 'Ooops!  Your upload triggered the following error:  ' . $_FILES[$file_id]['error'];
			return false;
		}
	}
}
?>