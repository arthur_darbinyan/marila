<?php
	require_once ('marila_api.php');
	
	$action = (!isset($_REQUEST["action"])) ? "" : $_REQUEST["action"];

	if($action) {
		
		$marilaApi = new MarilaApi();
		
		switch($action) {
			
			case 'SaveQuestion':
				$language = (!isset($_POST["language"])) ? "" : $_POST["language"];
				$data = (!isset($_POST["data"])) ? "" : $_POST["data"];
				echo $marilaApi->saveQuestion($language, $data);
				break;
				
			case 'AddQuestion':
				$data = (!isset($_POST["data"])) ? "" : $_POST["data"];
				echo $marilaApi->addQuestionAllLangs($data);
				break;
				
			case 'RemoveQuestion':
				$questionId = (!isset($_POST["questionId"])) ? "" : $_POST["questionId"];
				echo $marilaApi->removeQuestionAllLangs($questionId);
				break;
			
			case 'SaveMap':
				$lat = (!isset($_POST["centerLat"])) ? "" : $_POST["centerLat"];
				$lng = (!isset($_POST["centerLng"])) ? "" : $_POST["centerLng"];
				$zoom = (!isset($_POST["zoom"])) ? "" : $_POST["zoom"];
				$page = (!isset($_POST["page"])) ? "" : $_POST["page"];
				echo $marilaApi->saveMap($lat, $lng, $zoom, $page);
				break;
				
			case 'RemoveMarker':
				$lat = (!isset($_POST["lat"])) ? "" : $_POST["lat"];
				$lng = (!isset($_POST["lng"])) ? "" : $_POST["lng"];
				$page = (!isset($_POST["page"])) ? "" : $_POST["page"];
				echo $marilaApi->removeMarker($lat, $lng, $page);
				break;
			
			case 'AddMarker':
				$lat = (!isset($_POST["lat"])) ? "" : $_POST["lat"];
				$lng = (!isset($_POST["lng"])) ? "" : $_POST["lng"];
				$page = (!isset($_POST["page"])) ? "" : $_POST["page"];
				echo $marilaApi->addMarker($lat, $lng, $page);
				break;
			
			case 'RemoveVideo':
				$video_data = (!isset($_POST["videoData"])) ? "" : $_POST["videoData"];
				$page = (!isset($_POST["page"])) ? "" : $_POST["page"];
				echo $marilaApi->removeVideo($video_data, $page);
				break;
			
			case 'SaveStaticPageText':
				$language = (!isset($_POST["language"])) ? "" : $_POST["language"];
				$page = (!isset($_POST["page"])) ? "" : $_POST["page"];
				$text = (!isset($_POST["text"])) ? "" : $_POST["text"];
				echo $marilaApi->updateStaticPageText($language, $page, $text);
				break;
				
			case 'SaveSliderItem':
				$data = (!isset($_POST["sliderData"])) ? "" : $_POST["sliderData"];
				echo $marilaApi->editSlider($data);
				break;
			
			case 'RemoveSliderItem':
				$id = (!isset($_POST["id"])) ? "" : $_POST["id"];
				$index = (!isset($_POST["index"])) ? "" : $_POST["index"];
				$src = (!isset($_POST["src"])) ? "" : $_POST["src"];
				
				echo $marilaApi->removeSliderItem($id, $index, $src);
				break;
				
			case 'RemoveImage':
				$imgId = (!isset($_POST["imgId"])) ? "" : $_POST["imgId"];
				$page = (!isset($_POST["page"])) ? "" : $_POST["page"];
				
				echo $marilaApi->removeImage($imgId, $page);
				break;
				
			case 'DeleteProduct':
				$productId = (!isset($_POST["productId"])) ? "" : $_POST["productId"];
				$productType = (!isset($_POST["productType"])) ? "" : $_POST["productType"];
				
				echo $marilaApi->deleteProduct($productId, $productType);
				break;
		
			case 'AddVacancy':
				$data = (!isset($_POST["data"])) ? "" : $_POST["data"];
				
				echo $marilaApi->addVacancy($data);
				break;	
			
			case 'DeleteVacancy':
				$vacancyId = (!isset($_POST["vacancyId"])) ? "" : $_POST["vacancyId"];
				
				echo $marilaApi->deleteVacancy($vacancyId);
				break;	
			
			default:;
			
		}
	}
?>