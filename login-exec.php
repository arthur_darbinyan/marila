<?php
    //Start session
    session_start();

	require 'include.php';
    //Array to store validation errors
    $errmsg_arr = array();

    //Validation error flag
    $errflag = false;


    //Function to sanitize values received from the form. Prevents SQL injection
 if (isset($_POST['submit']) || isset($_SESSION['login'])) {
	// if the SESSION exists... (this is here if the submit button is pressed)
	if (isset($_SESSION['login'])) {
		// if the SESSION equls the MD5 hash...
		if ($_SESSION['login'] == $hash) {
			$_SESSION["login"] = $hash;
			//header("Location: $_SERVER[PHP_SELF]");
			header("location: home.php");
            exit();
			
		} else {
			header("location: home.php#admin");
            exit();
		}
	} else {
		//info
		//Sanitize the POST values
	    $user_email = $_POST['user'];
	    $pwd = $_POST['pass'];
	
	    //Input Validations
	    if($user_email == '') {
	        $errmsg_arr[] = 'Login ID missing';
	        $errflag = true;
	    }
	    if($pwd == '') {
	        $errmsg_arr[] = 'Password missing';
	        $errflag = true;
	    }
	
	    //If there are input validations, redirect back to the login form
	    if($errflag) {
	    	$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
	        session_write_close();
	        header("location: home.php#admin");
	        exit();
	    }
		// if the submit button is pressed, but the SESSION dose not exist, go through these steps...
		if ($_POST['user'] == $user && $_POST['pass'] == $pass){
			//user = your username & password = your password. acess granted. add SESSIONs and refresh.
			$_SESSION["login"] = $hash;
			//header("Location: $_SERVER[PHP_SELF]");
			header("location: home.php");
            exit();
		} else {
			 header("location: home.php#admin");
            exit();
		}
	}
} else {
	header("location: home.php#admin");
            exit();
}
?>
