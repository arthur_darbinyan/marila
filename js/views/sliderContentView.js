var sliderContentView = new View();
sliderContentView.initialize({
	templateId: "SliderContent",
	parentElementId: "slider_content"
});

sliderContentView.initilizeListeners = function() {
	loadSliderItems().done(function(items) {
		renderSlider(items);
	});
	
	sliderContentView.dontReloadHtmlOnLanguageChange = true;
	
	if(typeof SliderAdmin != "undefined" && SliderAdmin) {
		SliderAdmin.renderAdminUI();
	}
};