var productsMeatPage = new Page();
productsMeatPage.destroy = function() {
	productsMeatPage.getPage().destroy();
	$("#logo").attr("src", "img/logo.png");
	$("#top_header").css("background", "#075EAD");
	
	$(".menu-arrow").each(function() {
		$(this).attr("src", "img/arrow.png");
	});
	$('#shortcut_icon').attr("href", "img/shortcut-icon.png");
};
productsMeatPage.init([productsMeatView, menuView], [], "products_meat_page", function() {
		switch(window.currentLanguage) {
		case 'arm':
		window.document.title = "Marila - Մսամթերք";
		break;
		case 'en':
		window.document.title = "Marila - Meat Products";
		break;
		case 'ru':
		window.document.title = "Marila - Мясные Продукты";
		break;
	}
	
	$('#shortcut_icon').attr("href", "img/shortcut-icon-meat.png");

});