<?php

if(!isset($_SESSION)) {
	session_start();
}

require('_login_users.php');
require('_login_class.php');

$login = new login_class;

$today_ts = strtotime("now");
$today_m = date('n', $today_ts);
global $pass_login;


$login->domain_code = $domain_code;
$login->today_ts = $today_ts;
$login->today_m = $today_m;
$login->users = $users;
$login->num_1 = $random_num_1;
$login->num_2 = $random_num_2;
$login->num_3 = $random_num_3;


function isLoggedIn() {

	$pass_login = FALSE;
	global $domain_code;
	global $login;
	
	//Logged In
	if (isset($_SESSION[$domain_code.'_uid']) && $_SESSION[$domain_code.'_uid']!='' && isset($_SESSION[$domain_code.'_cid']) && $_SESSION[$domain_code.'_cid']!='') {
		
		$key_uid = $login->cleanse_input($_SESSION[$domain_code.'_uid']);
		$key_cid = $login->cleanse_input($_SESSION[$domain_code.'_cid']);
		
		if (!$login->verify_login($key_uid, $key_cid)) {
			$login->error_message = 'Login has expired';
		} else {
			$pass_login = TRUE;	
		}
	}
	return $pass_login;
}
?>