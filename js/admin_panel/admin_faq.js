function AdminFAQ() {

};

AdminFAQ.showEditItemPopup = function(question) {
	loadTemplate("templates/admin_panel/EditFaq.html").done(function(html) {
		
		$("#faq_popup").html(Mustache.to_html(html, {
            data: question,
            lang: window.lang
        }));

        $('#faq_popup').bPopup({
        	onClose: function() {
        		$('#faq_popup').html("");
            }
        });
      
        nicEditors.allTextAreas({buttonList: ['bold','italic', 'underline', 'left', 'center', 'right', 'justify', 'ol', 
			'ul', 'indent', 'outdent', 'link', 'unlink']});
        
        $("#save_question").click(function() {
        	var _data = AdminFAQ.validateEditData();
	        	
        	if(_data) {
        		_data.id = question.id;
        		AdminFAQ.saveQuestion(_data);
        	}
        });
	});
};

AdminFAQ.validateEditData = function() {
	
	var json = {}, 
		error = "",
		answer = $.trim(nicEditors.findEditor('question_answer').getContent());
		
	if($.trim($("#edit_faq .new_faq_title:first").val())=="") {
		error += "Please fill question title\n";
	} else {
		json.title = $.trim($("#edit_faq .new_faq_title:first").val());
	}
	
	if(answer == "" || answer == "<br>") {
		error += "Please fill question answer\n";
	} else {
		json.text = answer;
	}
	
	if($.trim(error)!=="") {
		alert(error);
		return false;
	} else {
		return json;
	}
};

AdminFAQ.showAddItemPopup = function() {
	loadTemplate("templates/admin_panel/AddFaq.html").done(function(html) {
		
		$("#faq_popup").html(Mustache.to_html(html, {
            data: {},
            lang: window.lang
        }));
  
        AdminFAQ.setLanguage();
        $('#faq_popup').bPopup({
        	onClose: function() {
        		$('#faq_popup').html("");
            }
        });
        nicEditors.allTextAreas({buttonList: ['bold','italic', 'underline', 'left', 'center', 'right', 'justify', 'ol', 
			'ul', 'indent', 'outdent', 'link', 'unlink']});
        
        $("#save_question").click(function() {
        	var _data = AdminFAQ.validateData();
	        	
        	if(_data) {
        		AdminFAQ.addQuestion(_data);
        	}
        });
	});
};

AdminFAQ.saveQuestion = function(_data) {
	$.ajax({
		type: "POST",
		url: "admin_panel/actions_controller.php",
		data: { 
			action: "SaveQuestion", 
			language: Localization.currentLanguage(), 
			data: JSON.stringify(_data)
		}
	}).done(function( msg ) {
		alert(msg);
		window.location.reload();
	}).fail(function(jqXHR, textStatus) {
		alert( "Request failed: " + textStatus );
	});
};

AdminFAQ.addQuestion = function(_data) {
	$.ajax({
		type: "POST",
		url: "admin_panel/actions_controller.php",
		data: { action: "AddQuestion", data: JSON.stringify(_data) }
	}).done(function( msg ) {
		alert(msg);
		window.location.reload();
	}).fail(function(jqXHR, textStatus) {
		alert( "Request failed: " + textStatus );
	});
};

AdminFAQ.validateData = function(isEdit) {
	var json = {
		"arm": {},
		"en": {},
		"ru": {},
	};
	var error = "";
	
	if($.trim($("#new_faq_arm .new_faq_title:first").val())=="") {
		error += "Please fill question title in Armenian\n";
	} else {
		json.arm.title = $.trim($("#new_faq_arm .new_faq_title:first").val());
	}
	
	var armAnswer = $.trim(nicEditors.findEditor('answer_arm').getContent());
	if(armAnswer == "" || armAnswer == "<br>") {
		error += "Please fill question answer in Armenian\n";
	} else {
		json.arm.text = armAnswer;
	}
	
	if($.trim($("#new_faq_en .new_faq_title:first").val())=="") {
		error += "Please fill question title in English\n";
	} else {
		json.en.title = $.trim($("#new_faq_en .new_faq_title:first").val());
	}
	
	var enAnswer = $.trim(nicEditors.findEditor('answer_en').getContent());
	if(enAnswer == "" || enAnswer == "<br>") {
		error += "Please fill question answer in English\n";
	} else {
		json.en.text = enAnswer;
	}
	
	if($.trim($("#new_faq_ru .new_faq_title:first").val())=="") {
		error += "Please fill question title in Russian\n";
	} else {
		json.ru.title = $.trim($("#new_faq_ru .new_faq_title:first").val());
	}
	
	var ruAnswer = $.trim(nicEditors.findEditor('answer_ru').getContent());
	
	if(ruAnswer == "" || ruAnswer == "<br>") {
		error += "Please fill question answer in Russian\n";
	} else {
		json.ru.text = ruAnswer;
	}
	
	if($.trim(error)!=="") {
		alert(error);
		return false;
	} else {
		return json;
	}
};

AdminFAQ.setLanguage = function() {
	SliderAdmin.currentLanguage = window.currentLanguage;
	$("#admin_language ." + SliderAdmin.currentLanguage + ":first").addClass("selected");
	$("#new_faq_" + SliderAdmin.currentLanguage).css("display", "block");
	
	$("#admin_language").delegate("span", "click", function() {
		$("#admin_language ." + SliderAdmin.currentLanguage + ":first").removeClass("selected");
		$(this).addClass("selected");
		$("#new_faq_" + SliderAdmin.currentLanguage).css("display", "none");
		$("#new_faq_" + $(this).attr("data-lang")).css("display", "block");
		SliderAdmin.currentLanguage = $(this).attr("data-lang");
	});
}

AdminFAQ.initializeView = function() {
	var addQuestion = $("<input type='button' class='red_gradient admin_add_button' style='margin-top: 40px' value='Add Question' />");
	$("#faq_div").append(addQuestion);
	
	addQuestion.click(function() {
		AdminFAQ.showAddItemPopup();
	})
};

AdminFAQ.renderRemoveEditButtons = function(div, question) {
	
	var deleteBtn = $('<img src="img/delete.png" title="Delete Quesion" class="faq_remove_edit"/>');
	var editBtn = $('<img src="img/edit.png" title="Edit Quesion"class="faq_remove_edit"/>');

	deleteBtn.insertBefore(div.find(".faq_answer:first"));
	editBtn.insertBefore(div.find(".faq_answer:first"));
	
	deleteBtn.click(function() {
		
	    var remove = confirm("Remove Question(will remove other languages as well)?");
		if(remove) {
			$.ajax({
				type: "POST",
				url: "admin_panel/actions_controller.php",
				data: { action: "RemoveQuestion", questionId: question.id }
			}).done(function( msg ) {
				alert(msg);
				window.location.reload(); 
			}).fail(function(jqXHR, textStatus) {
				alert( "Request failed: " + textStatus );
			});
		}
	});
	
	editBtn.click(function() {
		AdminFAQ.showEditItemPopup(question);
	});
};