var sliderView = new View();
loadSliderItems().done(function(items) {
	sliderView.initialize({
		templateId: "Slider",
		parentElementId: "slider",
		templateParameters: { items: items }
	});
})


sliderView.initilizeListeners = function() {
	this.dontRenderOnLanguageChange = true;
	
	$(document.body).css("overflow", "hidden")
	$("#slider_img_container div:nth-child(0)").css("width", $(window).width());
	$("#slider_img_container div:nth-child(1)").css("width", $(window).width());
	
	$("#slider_img_container div:nth-child(0)").css("height", $(window).height())
	$("#slider_img_container div:nth-child(1)").css("height", $(window).height());
	
	
};