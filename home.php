<?php
	session_start();
	header('X-Frame-Options: GOFORIT'); 
	require_once('admin_panel/home_actions.php');
	
	if(((isset($_GET['admin_login']) && $_GET['admin_login']) || 
		(isset($isAdminUrl) && $isAdminUrl)) || isLoggedIn()) {
		$isAdminUrl = true;
		require_once('_login.php');
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="initial-scale=1.0, user-scalable=yes" />
  		<meta property="og:title" content="Marila"/>
  		<meta id="wixMobileViewport" name="viewport" content="minimum-scale=0.25, maximum-scale=1.2"/>
		<meta name="keywords" content="Marila, Lamax, Կաթնամթերք, Մսամթերք, Սյունիքի Բարիքներ"/>
  		
  		<link id="shortcut_icon" rel="shortcut icon" href="img/shortcut-icon.png" type="image/png"/>	
  			
		<!-- libs -->
		<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
		<script type="text/javascript" src="js/libs/jquery.hoverIntent.minified.js"></script>
		<script type="text/javascript" src="js/libs/colorbox/jquery.colorbox-min.js"></script>
		<script type="text/javascript" src="js/libs/jquery.bpopup.min.js"></script>
		
		<script type="text/javascript" src="js/libs/jquery.subscribe.js"></script>
		<script type="text/javascript" src="js/libs/mustache-0.7.0.js"></script>
	
		<script type="text/javascript" src="js/localization.js"></script>
		<script type="text/javascript" src="js/cache/cache.js"></script>
		
		<!-- slider styles -->
		<link rel="stylesheet" type="text/css" href="css/wt-rotator.css"/>
		
		<!-- slider scripts -->
		<script type="text/javascript" src="js/ui/slider.js"></script>
		<script type="text/javascript" src="js/libs/jquery.easing.1.3.min.js"></script>
		<script type="text/javascript" src="js/libs/jquery.wt-rotator.js"></script>
		
		<script type="text/javascript" src="js/utils.js"></script>
		<!-- ui scripts -->
		<script type="text/javascript" src="js/ui/menu.js"></script>
		
		<!-- fonts -->
		<link href='http://fonts.googleapis.com/css?family=Belleza' rel='stylesheet' type='text/css'>
		
		<!-- for share -->
		<script type="text/javascript">var switchTo5x = true;</script>
		
		<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
		<script type="text/javascript" src="http://s.sharethis.com/loader.js"></script>
	
		<!-- external styles -->
		<link rel="stylesheet" type="text/css" href="css/wt-rotator.css"/>
		<link rel="stylesheet" href="css/colorbox.css" />
		
		<!-- internal styles -->
		<link href="css/main.css" rel="stylesheet"/>
		<link href="css/products.css" rel="stylesheet"/>
		<link href="css/product_detail.css" rel="stylesheet"/>
		<script type="text/javascript" src="js/languagesInit.js"></script>
		<script type="text/javascript" src="js/app.js"></script>
		<?php
			if(isLoggedIn() && isset($isAdminUrl) && $isAdminUrl) {
			//if($pass_login) {
				echo '<link href="css/admin.css" rel="stylesheet"/>';
				echo '<script type="text/javascript" src="js/admin_panel/admin_logout.js"></script>';
				
				if($isAdmin) {
				//	echo '<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />';
 			 	//	echo '<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>';
  					echo '<script type="text/javascript" src="js/admin_panel/admin_utils.js"></script>';
					echo '<script type="text/javascript" src="http://js.nicedit.com/nicEdit-latest.js"></script>';
					echo '<script type="text/javascript" src="js/admin_panel/admin.js"></script>';
					echo '<script type="text/javascript" src="js/admin_panel/admin_home.js"></script>';
					echo '<script type="text/javascript" src="js/admin_panel/admin_staticpage.js"></script>';
					echo '<script type="text/javascript" src="js/admin_panel/admin_video.js"></script>';
					echo '<script type="text/javascript" src="js/admin_panel/admin_where_to_buy.js"></script>';
					echo '<script type="text/javascript" src="js/admin_panel/admin_contact_us.js"></script>';
					echo '<script type="text/javascript" src="js/admin_panel/admin_faq.js"></script>';
					echo '<script type="text/javascript" src="js/libs/autosize-master/jquery.autosize-min.js"></script>';
				}
			}
		?>
		<!-- views -->
		<script type="text/javascript" src="js/views/view.js"></script>
		<script type="text/javascript" src="js/views/topHeaderView.js"></script>
		<script type="text/javascript" src="js/views/menuView.js"></script>
		<script type="text/javascript" src="js/views/productsView.js"></script>
		<script type="text/javascript" src="js/views/productsMeatView.js"></script>
		<script type="text/javascript" src="js/views/sliderView.js"></script>
		<script type="text/javascript" src="js/views/sliderContentView.js"></script>
		<script type="text/javascript" src="js/views/whereToBuyView.js"></script>
		<script type="text/javascript" src="js/views/historyView.js"></script>
		<script type="text/javascript" src="js/views/achievementsView.js"></script>
		<script type="text/javascript" src="js/views/imagesCompanyView.js"></script>
		<script type="text/javascript" src="js/views/imagesMeetingsView.js"></script>
		<script type="text/javascript" src="js/views/videosView.js"></script>
		<script type="text/javascript" src="js/views/videosMeetingsView.js"></script>
		<script type="text/javascript" src="js/views/contactUsView.js"></script>
		<script type="text/javascript" src="js/views/adminView.js"></script>
		<script type="text/javascript" src="js/views/faqView.js"></script>
		<script type="text/javascript" src="js/views/careersView.js"></script>
		<script type="text/javascript" src="js/views/ourBenefitsView.js"></script>
		
		<!-- pages -->
		<script type="text/javascript" src="js/pages/page.js"></script>
		<script type="text/javascript" src="js/pages/homePage.js"></script>
		<script type="text/javascript" src="js/pages/productsPage.js"></script>
		<script type="text/javascript" src="js/pages/productsMeatPage.js"></script>
		<script type="text/javascript" src="js/pages/whereToBuyPage.js"></script>
		<script type="text/javascript" src="js/pages/historyPage.js"></script>
		<script type="text/javascript" src="js/pages/achievementsPage.js"></script>
		<script type="text/javascript" src="js/pages/imagesCompanyPage.js"></script>
		<script type="text/javascript" src="js/pages/imagesMeetingsPage.js"></script>
		<script type="text/javascript" src="js/pages/videosPage.js"></script>
		<script type="text/javascript" src="js/pages/videosMeetingsPage.js"></script>
		<script type="text/javascript" src="js/pages/contactUsPage.js"></script>
		<script type="text/javascript" src="js/pages/adminPage.js"></script>
		<script type="text/javascript" src="js/pages/faqPage.js"></script>
		<script type="text/javascript" src="js/pages/careersPage.js"></script>
		<script type="text/javascript" src="js/pages/ourBenefitsPage.js"></script>
		
		<script type="text/javascript" src="js/routing.js"></script>
		
		<script type="text/javascript" src="js/app.js"></script>

	</head>
	<body>
		<a href="."><img src="img/logo.png"/ id="logo"/></a>
		<div id="top_header" class="top_header">
			<div id="fb-root"></div>
			<div class="fb-like facebook_like" data-href="https://www.facebook.com/MarilaLLC" data-send="false" data-layout="button_count" data-width="450" data-show-faces="false"></div>
			<div id="top_header_content"></div>
		</div>
		<div id="menu_div"></div>
		
		<div id="slider"></div>
		<div id="page_content" class="content_font"></div>
					
		<div id="products_popup" style="display:none">
			<img class="close_popup" src="img/close.png" />
			<div id="popup_content">
			</div>
		</div>
		
		<div id="career_popup" style="display:none">
			<img class="close_popup" src="img/close.png" />
			<div id="career_popup_content">
			</div>
		</div>
	
		<?php
	//	if(isLoggedIn() || !(isset($isAdminUrl) && $isAdminUrl)) {
		?>
			<div id="slider_popup" style="display:none">
				<img class="close_popup" src="img/close.png" />
				<div id="slider_popup_content">
				</div>
			</div>
			
			<div id="video_popup" style="display:none">
				<img class="close_popup" src="img/close.png" />
				<div id="video_popup_content">
				</div>
			</div>
			
			<div id="faq_popup" style="display:none">
				<img class="close_popup" src="img/close.png" />
				<div id="faq_popup_content">
				</div>
			</div>	
		<?php	
	//	}	
		?>
		
		<script type="text/javascript" src="js/share.js"></script>
	</body>
</html>