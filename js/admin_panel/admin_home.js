function SliderAdmin() {

};

SliderAdmin.loadCurrentItemDetails = function() {
	var deferred = $.Deferred();
	var data = {};
	loadedItems = 0;
	$.each(Localization.languages, function(i, lanaguage) {
		$.ajaxSetup({ cache: false });
		$.get('data/slider/content-' + lanaguage + '.xml', function(_data) {
			$(_data).find('item').each(function() {
				if($(this).attr("index") == (slider.getItems()[slider.getCurrentIndex()]).index) {

					var $item = $(this);
					++loadedItems;
					data.id = $item.attr("id");
					data.href = $item.attr("href")
					data[lanaguage] = ({
						title: $item.find("title:first").text(),
						text: $item.find("text:first").text()
					});
					
					if(loadedItems == Localization.languages.length) {
						deferred.resolve(data);
						
						$.ajaxSetup({ cache: true });
					}	
				}
			});
		});
		$.ajaxSetup({ cache: true });
	});
	return deferred;
}

SliderAdmin.showEditItemPopup = function(obj ) {
	loadTemplate("templates/admin_panel/EditSliderItem.html").done(function(html) {
		SliderAdmin.loadCurrentItemDetails().done(function(data) {
	
			$("#slider_popup_content").html(Mustache.to_html(html, {
	            data: data,
	            lang: window.lang
	        }));
	        
            $('.slider_text').autosize();
	        SliderAdmin.setLanguage();
	        $('#slider_popup').bPopup({});
	        
	        $("#edit_slider").click(function() {
	        	var _data = SliderAdmin.validateData(true);
	        	
	        	if(_data) {
	        		_data.id = data.id;
	        		SliderAdmin.saveSliderItem(_data);
	        	}
	        });
		});
	});
};

SliderAdmin.saveSliderItem = function(data) {

	$.ajax({
		type: "POST",
		url: "admin_panel/actions_controller.php",
		data: { action: "SaveSliderItem", sliderData: JSON.stringify(data) }
	}).done(function( msg ) {
		window.location.reload(); 
	}).fail(function(jqXHR, textStatus) {
		alert( "Request failed: " + textStatus );
	});
};


SliderAdmin.showAddNewItemPopup = function(obj ) {

	loadTemplate("templates/admin_panel/AddSliderItem.html").done(function(html) {
		
		var sliderValues = [];
		for(var i=1; i<=slider.getItems().length + 1; i++) {
			sliderValues.push({val: i});
		}
		
		$("#slider_popup_content").html(Mustache.to_html(html, {
            data: { sliderValues: sliderValues },
            lang: window.lang
        }));
        
       
        $("#slider_item_index option").filter(function() {
		    return $(this).attr('value') == slider.getItems().length + 1;
		}).attr('selected', true);
				
        $('.slider_text').autosize();
        SliderAdmin.setLanguage();
        $('#slider_popup').bPopup({});
        
        $("#add_new_slider").click(function() {
        	var data = SliderAdmin.validateData(false);
        	if(data) {
        		data.index = $("#slider_item_index").val();
        		SliderAdmin.addSliderItem(JSON.stringify(data));
        	}
        });
        
        		
		var url = window.URL || window.webkitURL;	
		$("#slider_popup .slider_img:first").change(function(e) {
			var self = $(this);
		    if( this.disabled ) {
		        alert('Your browser does not support File upload.');
		    }else {
		        var chosen = this.files[0];
		        var image = new Image();
		        image.onload = function() {
		        	if(this.width != 1700 || this.height != 1200) {
		        		alert("Invalid image size - " + 'Width: '+ this.width +'px, Height: '+ this.height + "px\n"
		        		+ "Image size should be - 1700 X 1200" );
		        		self.val("")
		        	}
		        };
		        image.onerror = function() {
		            alert('Not a valid file type: '+ chosen.type);
		            self.val("");
		        };
		        image.src = url.createObjectURL(chosen);                    
		     }
		});
 	 });
};

SliderAdmin.setLanguage = function() {
	SliderAdmin.currentLanguage = window.currentLanguage;
	$("#admin_language ." + SliderAdmin.currentLanguage + ":first").addClass("selected");
	$("#new_slider_" + SliderAdmin.currentLanguage).css("display", "block");
	
	$("#admin_language").delegate("span", "click", function() {
		
		$("#admin_language ." + SliderAdmin.currentLanguage + ":first").removeClass("selected");
		$(this).addClass("selected");
		$("#new_slider_" + SliderAdmin.currentLanguage).css("display", "none");
		$("#new_slider_" + $(this).attr("data-lang")).css("display", "block");
		SliderAdmin.currentLanguage = $(this).attr("data-lang");
	});
}


SliderAdmin.validateData = function(isEdit) {
	var json = {
		"arm": {},
		"en": {},
		"ru": {},
	};
	var error = "";
	
	if(!isEdit) {
		if($("#slider_popup .slider_img:first").val()=="") {
			error += "Please upload image \n";
		} else if(!isImage($("#slider_popup .slider_img:first").val())) {
			error += "Invalid File: Please upload png or jpg files\n";
		}
	}
	if($.trim($("#slider_popup .new_slider_reference:first").val())=="") {
		error += "Please fill page reference\n";
	} else {
		json.href = $.trim($("#slider_popup .new_slider_reference:first").val());
	}
	
	if($.trim($("#new_slider_arm .new_slider_title:first").val())=="") {
		error += "Please fill title in armenian\n";
	} else {
		json.arm.title = $.trim($("#new_slider_arm .new_slider_title:first").val());
	}
	
	if($.trim($("#new_slider_arm .new_slider_text:first").val())=="") {
		error += "Please fill news text in armenian\n";
	} else {
		json.arm.text = $.trim($("#new_slider_arm .new_slider_text:first").val());
	}
	
	if($.trim($("#new_slider_en .new_slider_title:first").val())=="") {
		error += "Please fill title in english\n";
	} else {
		json.en.title = $.trim($("#new_slider_en .new_slider_title:first").val());
	}
	
	if($.trim($("#new_slider_en .new_slider_text:first").val())=="") {
		error += "Please fill news text in english\n";
	} else {
		json.en.text = $.trim($("#new_slider_en .new_slider_text:first").val());
	}
	
	if($.trim($("#new_slider_ru .new_slider_title:first").val())=="") {
		error += "Please fill title in russian\n";
	} else {
		json.ru.title = $.trim($("#new_slider_ru .new_slider_title:first").val());
	}
	
	if($.trim($("#new_slider_ru .new_slider_text:first").val())=="") {
		error += "Please fill news text in russian\n";
	} else {
		json.ru.text = $.trim($("#new_slider_ru .new_slider_text:first").val());
	}
	
	if($.trim(error)!=="") {
		alert(error);
		return false;
	} else {
		return json;
	}
};

SliderAdmin.addSliderItem = function(data) {
	$("#new_slider_data").val(data);
	$("#add_slider_form").submit();
};

SliderAdmin.removeCurrentItem = function() {
	
	$.ajax({
		type: "POST",
		url: "admin_panel/actions_controller.php",
		data: { action: "RemoveSliderItem", index: slider.getCurrentItem().index, id: slider.getCurrentItem().id, src: slider.getCurrentItem().src }
	}).done(function( msg ) {
		window.location.reload(); 
	}).fail(function(jqXHR, textStatus) {
		alert( "Request failed: " + textStatus );
	});
};

SliderAdmin.renderAdminUI = function() {
	if($("#slider_pause").length != 0) {
		return;
	}
	var slider_pause = $('<img src="img/pause.png" class="pointer" id="slider_pause"/>');
	var remove = $("<div><input type='button' class='red_gradient admin_add_button' value='Remove Current Item' /><div>");
	var edit = $("<div><input type='button' class='orange_gradient admin_add_button' value='Edit Current Item' /><div>");
	var add = $("<div><input type='button' class='green_gradient admin_add_button' value='Add New Item' /><div>");
	
	$("#slider_a_ui").append(slider_pause);
	$("#slider_a_ui").append(remove);
	$("#slider_a_ui").append(edit);
	$("#slider_a_ui").append(add);
	
	slider_pause.click(function() {
		if($(this).attr("src")=="img/pause.png") {
			$(this).attr("src", "img/play.png");
		} else {
			$(this).attr("src", "img/pause.png");
		}
		$("#pause_slider").trigger("click");
	});
	
	add.click(function() {
		SliderAdmin.showAddNewItemPopup();
	});
	
	edit.click(function() {
		SliderAdmin.showEditItemPopup();
	});
	
	remove.click(function() {
		var remove = confirm("Delete current item?");
		if(remove) {
			SliderAdmin.removeCurrentItem();
		}
	});
	
	$("#slider_popup .close_popup").click(function() {
		$('#slider_popup').bPopup().close();
	});

};