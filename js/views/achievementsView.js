var achievementsView = new View();
achievementsView.initialize({
	templateId: "Achievements",
	parentElementId: "page_content",
	templateDependsOnLanguage: true
});

achievementsView.initilizeView = function(categoryId) {
	if(typeof StaticPageAdmin != "undefined" && StaticPageAdmin) {
		StaticPageAdmin.initializeAchievementsView();
	}
};

achievementsView.initilizeListeners = function() {
	this.initilizeView();
};