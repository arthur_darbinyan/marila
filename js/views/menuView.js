var menuView = new View();
menuView.initialize({
	templateId: "Menu",
	parentElementId: "menu_div"
});

var menu = new Menu();

menuView.initilizeListeners = function() {
	menuView.dontRerenderOnPageChange = true;
	menu.init(["menu_milk_products", "menu_meat_products", "menu_aboutus", "menu_videos", 
		"menu_images", "menu_careers"]);
		
	$("#buy").click(function(){
		window.location = "#buy_centers";
	});
	menuView.selectMenu();
	
	if(window.currentpage == 'products_meat_page') {
		$(".menu-arrow").each(function() {
			$(this).attr("src", "img/arrow-meat.png");
		});
	}
};

menuView.selectMenu = function() {
	var dontSelect = false;

	switch(window.currentpage) {
		case "products_milk_page":
			this.activeHeader = $($('#menu_milk_products .text')[0]);
			break;
		case "products_meat_page":
			this.activeHeader = $($('#menu_meat_products .text')[0]);
			break;
		case "history_page": 
		case"achievements_page":
			this.activeHeader = $($('#menu_aboutus .text')[0]);
			break;
		case "videos_page":
			this.activeHeader = $($('#menu_videos .text')[0]);
			break;
		case "images_meetings_page":
		case "images_page":
			this.activeHeader = $($('#menu_images .text')[0]);
			break;
		case "careers_page":
			this.activeHeader = $($('#menu_careers .text')[0]);
			break;
		default: {
			if(this.activeHeader) {
				this.activeHeader.css("background", "");
				dontSelect = true;
			}
		}
	}
	if(this.activeHeader && !dontSelect) {
		this.activeHeader.css("background", "white");
	}
}
