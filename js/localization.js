var Localization = {
	
	// localStorage or Cookie key
	cacheKey: "marilla-language",
	languages: ["arm", "en", "ru"],
	defaultLanguage: "arm",
	
	currentLanguage: function() {
		var cacheVal = cache.getItem(this.cacheKey);
		return cacheVal ? cacheVal : this.defaultLanguage;
	},
	
	loadLanguageFile : function(lang) {
		window.lang = {};
		$.getJSON('lang/' + lang + '.json', function(data) {
			// ovewrite window.lang with the customization
			var part;
			for (part in data) {
				if (data.hasOwnProperty(part)) {
					window.lang[part] = data[part];
				}
			}
			if(window.languageInitialized) {
				$.publish("LanguageChanged", []);
			} else {
				$.publish("LanguageInitilized",[]);
			}
			window.languageInitialized = true;
		});
	},
	
	setLanguage : function(lang) {
		// save in cache
		window.currentLanguage = lang;
		cache.setItem(this.cacheKey, window.currentLanguage);
		this.loadLanguageFile(lang);
	}
};