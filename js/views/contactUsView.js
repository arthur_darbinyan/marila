var contactUsView = new View();
contactUsView.initialize({
	templateId: "ContactUs",
	parentElementId: "page_content",
	templateDependsOnLanguage: true
});

contactUsView.initilizeView = function(categoryId) {

    var map, markers = [];
	
	loadMapData('data/contactUsPage/map.xml').done(function(mapData) {
	
		var center = new google.maps.LatLng(mapData.center.lat, mapData.center.lng);
	    var options = {
		    zoom: parseInt(mapData.zoom),
		    center: center,
		    mapTypeId: google.maps.MapTypeId.ROADMAP
	    };
	    map = new google.maps.Map(document.getElementById("map_canvas"), options);
	    
	    $.each(mapData.markers, function(i, marker) {
	    	markers.push(new google.maps.Marker({ 
	    		position: new google.maps.LatLng(marker.lat, marker.lng), 
	    		map: map, 
	    		title: "Market" 
	    	}));
	    });
	    
	    if(typeof AdminContactUs != "undefined" && AdminContactUs) {
			AdminContactUs.initializeView(map, markers);
		}
	});
};

contactUsView.initilizeListeners = function() {
	this.initilizeView();
	menuView.selectMenu();
};