var imagesMeetingsView = new View();
imagesMeetingsView.initialize({
	templateId: "ImagesMeetings",
	parentElementId: "page_content"
});

imagesMeetingsView.initilizeView = function(categoryId) {
	var self = this;
	self.getImages().done(function(data) {
		self.renderImages(data);
		$(".group1").colorbox({rel:'group1', maxWidth: "85%", maxHeight: "90%"});
	});
	if(typeof ImagesAdmin!="undefined" && ImagesAdmin) {
		imagesAdmin.renderAdminUI("meetings");
	}
};

imagesMeetingsView.initilizeListeners = function() {
	this.initilizeView();
};

imagesMeetingsView.getImages = function() {
	var urls = [];
	var deferred = $.Deferred();
	$.ajaxSetup({
		cache: false
	});
	$.get('data/imagesPage/meetings/data.xml', function(data) {
		
		$(data).find('img').each(function() {
			var $img = $(this);
			urls.push({smallURL: $img.attr("smallURL"), bigURL: $img.attr("bigURL"), id: $img.attr("id")});
		});
		deferred.resolve(urls);
	});
	return deferred;
}

imagesMeetingsView.renderImages = function(urls) {
	$("#images_container").html("");
	for(var i=0; i<urls.length/4; i++) {
		var row = $("<div class='mtop25' style='float: left; width: 100%;'>");
		$("#images_container").append(row);
		for(var j=i*4; j<i*4+4; j++) {
			if(!urls[j]) {
				return;
			}
			var img;
			if(j%4===0) {
				img = $('<div style="float: left;"><a  class="group1"  href="' + urls[j].bigURL + '">'+
				'<img data-id="' + urls[j].id + '" src="' + urls[j].smallURL + '"style="height: 187px" alt="project" /></a></div>');
			} else {
				img = $('<div style="float: left; margin-left: 20px" ><a class="group1" href="' 
				+ urls[j].bigURL + '">' + '<img data-id="' + urls[j].id + '" src="'
				+ urls[j].smallURL + '"style="height: 187px" alt="project" /></a></div>');
			}
			$(row).append(img);
			if(typeof ImagesAdmin!="undefined" && ImagesAdmin) {
				ImagesAdmin.addAdminUI($(img), urls[j].id, "meetings");
			}
		}
	}
}