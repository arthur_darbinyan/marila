function Menu() {

	var activeId = null, activeHeader, callMouseOut = false, timer, callMouseOver = true,
	isInitialized = false;

	this.init = function(items) {
	
		if (typeof console === "undefined" || typeof console.log === "undefined") {
			console = {};
			console.log = function() {};
		}
		isInitialized = true;
		for (var i = 0; i < items.length; i++) {

			(function(index) {
				$('#' + items[index]).click(function() {
					//console.log($($('#' + items[index] + " a")[0]).attr('href'))
					window.location.href = $($('#' + items[index] + " a")[0]).attr('href');
					//	$($('#' + items[index] + " a")[0]).trigger("click");
					//alert($(this).find('> .a:first').attr("href"));
					callMouseOver = false;
					if(menuView.activeHeader &&
						menuView.activeHeader.attr('id') !== $($('#' + items[index] + " .text")[0]).attr('id')) {
						menuView.activeHeader.css("background", "");
						menuView.activeHeader = $($('#' + items[index] + " .text")[0]);
					}
					mouseOut(true);
				});
				$('#' + items[index]).hoverIntent(function() {
					if(!callMouseOver) {
						return;
					}
					callMouseOut = false;
					clearTimeout(timer);
					if (activeId && activeHeader) {
						$(activeId).css("visibility", "hidden");
						
						if(!menuView.activeHeader || activeHeader.attr("id") !== menuView.activeHeader.attr("id")) {
							activeHeader.css("background", "");
						}
					}
					activeHeader = $($('#'+items[index]+ " .text")[0]);
					activeHeader.css("background", "white");
					var id = '#' + items[index] + '_content';
					//$(id).css("background", "white");
					//$(id).addClass("red_gradient");
					$(id).css("top", "120px");

					if (index == 0) {
						$(id).css("left", $(this).offset().left - 10);
					} else if (index == 1) {
						$(id).css("left", $(this).offset().left - 120);
					} else {
						$(id).css("left", $(this).offset().left - (parseInt($(id).width()) -
						 parseInt($(this).width())) / 2 - 10);
					}

					var arrowPos = $($( '#'+items[index] + " .menu-arrow")[0]).offset();
					$("#arrow_top").css("top", arrowPos.top + 15);
					$("#arrow_top").css("left", arrowPos.left);
					$("#arrow_top").css("display", "block");
					
					$(id).css("visibility", "visible");
					activeId = id;
				}, function() {
					callMouseOut = true;
					callMouseOver = true;
					timer = setTimeout(mouseOut, 1200);
				});
	
				function mouseOut(dontCheckMouseOut) {
					if (!callMouseOut && !dontCheckMouseOut) {
						clearTimeout(timer);
						return;
					}
					$(activeId).css("visibility", "hidden");
					
					// Monkey code :P
					try {
						if(!menuView.activeHeader || 
								activeHeader.attr("id") !== menuView.activeHeader.attr("id")) {
							activeHeader.css("background", "");
						}
					} catch(e) {
						
					}
					$("#arrow_top").css("display", "none");
				}
			})(i);
		}

		$(".menu_content div").each(function() {
			$(this).bind("mouseover mouseout", function(event) {
				$(this).toggleClass("selected_menu");
			});
		});
	}
};