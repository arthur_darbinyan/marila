var productsMeatView = new View();

productsMeatView.initialize({
	templateId: "ProductsMeat",
	parentElementId: "page_content"
});

productsMeatView.renderProducts = function(categoryId) {

	productsMeatView.getProducts(categoryId).done(function(products) {
		$.each(products, function(index, product) {
			var left = "product_item_left";
			if(index % 3 == 0) {
				left = "";
			}
			
			var productDiv = $(
				'<div id="' + "product" + product.id + '" class="product_item product_item_shadow gradient product_item_top ' + left + ' left "'
				+ 'style="position:relative; text-align: center;">'
				+ '<span class="product_item_title">' + product["title-" + window.currentLanguage]  + '</span>'
				+ '<img src="' + product.dataIcon + '"' + " id='product_image_" + product.id + "'"
				+ "style='" +  (product.dataIconWidth ? "width: " +  product.dataIconWidth + "px" : "")
				+ "; max-width: 180px; max-height: 180px; margin:0px auto;'/></div>");
			
			$("#product_items").append(productDiv);
			
			if(typeof ProductsAdmin != "undefined" && ProductsAdmin) {
				ProductsAdmin.renderRemoveEditButtons(productDiv, product, categoryId, "meat");
			}
			
			document.getElementById("product_image_" + product.id).onload = function() {	
				$(this).css("margin-top", (240 - parseInt($(this).height())) / 2  + 20 + "px");
			};
			
			productDiv.click(function() {
				$("#popup_content").html("");
				
			
				loadTemplate("templates/product_details/EmptyTemplate.html").done(function(html) {
					$("#popup_content").html(Mustache.to_html(html, {
		                data: {},
		                lang: window.lang
		            }));
		            $('#products_popup').bPopup({});
			        $.getJSON(product.dataDetails, function(data) {

						$("#description_div").html(data[window.currentLanguage].description.replace(/\n/g, "<br>"));
	
						var productImage = $('<img src="' + data.image + '"' +
						' style="float:left; position: absolute; bottom: 0; max-width: 280px; max-height: 280px;"/>');
		
						$("#product_image_div").append(productImage);
						$(productImage).load(function() {
							$("#product_image_div").css("margin-left", (380 - parseInt(productImage.width())) / 2 - 10 + "px");
						})
						
						for(var i=0; i < data[window.currentLanguage].sectionsLength; i++) {
							var section = $('<div class="section"></div>');
				
							if(data[window.currentLanguage]["section" + i].length == 0 ||
								(data[window.currentLanguage]["section" + i].length == 1 && 
								$.trim(data[window.currentLanguage]["section" + i][0].val)=="")) {
								continue;
							}
							$("#product_sections").append(section);
							$.each(data[window.currentLanguage]["section" + i], function(j, row) {
								var row = $('<div class="row">' + data[window.currentLanguage]["section" + i][j].val + '</div>');
								$(section).append($(row));
								if(j%2==0) {
									row.css("background", "#F7F8F9");
								}			
							});
						}
						
						$("#product_title").html(data[window.currentLanguage].title);
					
					});
				});
			});
		});
	});
};

productsMeatView.initilizeView = function(categoryId) {
	
	productsMeatView.renderProducts(categoryId);

	$(".products_menu > div").each(function() {
		if($(this).attr("data-product-category") == categoryId) {
			$(this).addClass("selected");
			$(this).css("background", "#A42322")
			$($(this).children()[0]).css("color", "white");
			$(this).css("border-color", "#A42322");
		}
	});

	$("#logo").attr("src", "img/logo-meat.png");
	$("#top_header").css("background", "#8A1A19");
	
	if(categoryId != "all" && typeof ProductsAdmin != "undefined" && ProductsAdmin) {
		ProductsAdmin.renderAddProductButton(categoryId, "meat");
	}
};

productsMeatView.getProducts = function(categoryId) {
	var products = [];
	var deferred = $.Deferred();
	$.ajaxSetup({
		cache: false
	});
	
	$.get('data/meatProductsPage/data.xml', function(data) {
		$(data).find('product').each(function() {
			if($(this).parent().attr("type-id") == categoryId || categoryId == "all") {
				var $product = $(this);
				products.push({
					id: $product.attr("id"),
					dataIcon: $product.attr("data-icon"),
					dataIconWidth: $product.attr("data-icon-width"),
					dataDetails: $product.attr("data-details"),
					'title-en': $product.attr("title-en"),
					'title-ru': $product.attr("title-ru"),
					'title-arm': $product.attr("title-arm")
				});
			}
		});
		deferred.resolve(products);
	});
	return deferred;
};

productsMeatView.initilizeListeners = function(parameters) {
	this.initilizeView(parameters.id);

	$("#products_popup .close_popup").click(function() {
		$('#products_popup').bPopup().close();
	});
	
	$(".products_menu > div").click(function() {
		var productCategory = $(this).attr("data-product-category");
		window.location = "#products_meat?id=" + productCategory;
	});
	
	$(".menu-arrow").each(function() {
		$(this).attr("src", "img/arrow-meat.png");
	});
};