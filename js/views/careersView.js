var careersView = new View();
careersView.initialize({
	templateId: "Careers",
	parentElementId: "page_content",
	templateDependsOnLanguage: true
});

careersView.initilizeListeners = function() {
	this.initilizeView();
	
	$("#career_popup .close_popup").click(function() {
		$('#career_popup').bPopup().close();
	});
};

careersView.initilizeView = function() {
	careersView.getCareers().done(function(careers) {
		careersView.renderCareers(careers);
	});

	if(typeof CareersAdmin != "undefined" && CareersAdmin) {
		CareersAdmin.initializeView();
	}
};

careersView.renderCareers = function(careers) {
	$("#careers").html("");
	if(careers.length == 0) {
		$("#careers").html("<p style='margin-top: 5px'>At this time we don't have open vacancies.</p>");
	} else {
		$.each(careers, function(index, career) {
			var itemDiv = $("<div data-id='" + career.id + "'><span class='career_item'>" + career["title-" + window.currentLanguage] + "</span></div>");
			$("#careers").append(itemDiv);
			
			if(typeof CareersAdmin != "undefined" && CareersAdmin) {
				CareersAdmin.renderRemoveVacancyButton(itemDiv);
			}
			
			$(itemDiv).find(".career_item:first").click(function() {
				
				loadTemplate("templates/EmptyCareer.html").done(function(html) {
					$("#career_popup_content").html(Mustache.to_html(html, {
		                data: {},
		                lang: window.lang
		            }));
		            
					var fileName = "data/careersPage/vacancy-" + career.id + ".json"
					$.getJSON(fileName, function(data) {
						$("#job_title").html(data[window.currentLanguage].title.replace(/\n/g, "<br>"));
						$("#job_responsibilities").html(data[window.currentLanguage].responsibilities.replace(/\n/g, "<br>"));
						$("#job_qualifications").html(data[window.currentLanguage].description.replace(/\n/g, "<br>"));
						
						$('#career_popup').bPopup({});
					});
				});
			});
		});
	}
};

careersView.getCareers = function() {
	var careers = [];
	var deferred = $.Deferred();
	$.ajaxSetup({
		cache: false
	});
	
	$.get('data/careersPage/data.xml', function(data) {
		$(data).find('career').each(function() {
			var $career = $(this);
			careers.push({
				id: $career.attr("id"),
				'title-en': $career.attr("title-en"),
				'title-ru': $career.attr("title-ru"),
				'title-arm': $career.attr("title-arm")
			});
		});
		deferred.resolve(careers.reverse());
	});
	return deferred;
};