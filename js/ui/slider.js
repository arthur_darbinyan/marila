function SliderView() {
	var currentIndex = 0, self = this, changeFunc, rotator, items, numberOfCalls = 0;
	//sliderReferences = ["#products_milk?id=milk", "#products_milk?id=milk", 
	//"#products_milk?id=curd", "#products_milk?id=cheese"];
	
	
	this.init = function(_items, sliderLoaded) {
		items = _items;
		if(!sliderLoaded) {
			initSlider();
		} else {
			self.changeText(currentIndex, false)
		}
	};
	
	this.setItems = function(_items) {
		items = _items;
	};
	
	this.getItems = function() {
		return items;
	};
	
	this.getCurrentIndex = function() {
		return currentIndex;
	};
	
	this.getCurrentItem = function() {
		return items[currentIndex];
	};

	this.imageChanged = function(index) {
		$($("#slider_index div")[currentIndex]).css("background", "#BDB3AA")
		$($("#slider_index div")[index]).css("background", "#F04447");

		self.changeText(index);
		currentIndex = index;
		numberOfCalls++;
		
		$("#read_more").click(function() {
			window.location = items[index].href;
		});
	};

	function initSlider() {
		self.imageChanged(currentIndex);
	};

	this.changeText = function(index, animate) {
		if (arguments.length == 1) {
			animate = true;
		}

		if (numberOfCalls >= 2) {
			if(animate) {
				$("#slider_tex_container").animate({
					opacity : 0
				}, 500, function() {
					$("#slider_title").html(items[index].title);
					$("#slider_text").html(items[index].text);
					$("#slider_tex_container").animate({
						opacity : 1
					}, 400);
				});
			} else {
				$("#slider_title").html(items[index].title);
				$("#slider_text").html(items[index].text);
			}
		} else {
			$("#slider_title").html(items[index].title);
			$("#slider_text").html(items[index].text);
			
			if (animate) {
				$("#slider_tex_container").animate({
					opacity : 1
				}, 600);
			}
		}
	};

	return this;
};


function initSlider(items) {
	
	for(var i=0; i<items.length; i++ ) {
		$("#slider_index").append('<div class="slider_bubble"></div>');
	}
	$("#slider_index").css("width", items.length * 22 + 12 + "px" )

	var slider = $(".container").wtRotator({
		width : $(window).width(),
		height : $(window).height(),
		thumb_width : 24,
		thumb_height : 24,
		button_width : 24,
		button_height : 24,
		button_margin : 5,
		auto_start : true,
		delay : 7000,
		play_once : false,
		transition : "fade",
		transition_speed : 800,
		auto_center : true,
		easing : "",
		cpanel_position : "inside",
		cpanel_align : "BR",
		timer_align : "top",
		display_thumbs : true,
		display_dbuttons : true,
		display_playbutton : true,
		display_thumbimg : false,
		display_side_buttons : false,
		display_numbers : true,
		display_timer : false,
		mouseover_select : false,
		mouseover_pause : false,
		cpanel_mouseover : false,
		text_mouseover : false,
		text_effect : "fade",
		text_sync : true,
		tooltip_type : "text",
		shuffle : false,
		block_size : 75,
		vert_size : 55,
		horz_size : 50,
		block_delay : 25,
		vstripe_delay : 75,
		hstripe_delay : 180,
		mousewheel_scroll : false
	});
	
	$(".screen >a").removeAttr("href");
};

var sliderLoaded = false;

function renderSlider(items) {
	if(!sliderLoaded) {
		slider.init(items, sliderLoaded);
		initSlider(items);
		slider.initSliderListeners();
		sliderLoaded = true;
	} else {
		slider.setItems(items);
		slider.changeText(slider.getCurrentIndex(), false);
	}
};

$(window).bind('resize.slider', function() {
	$("#slider_img_container div:nth-child(0)").css("width", $(window).width());
	$("#slider_img_container div:nth-child(1)").css("width", $(window).width());
	
	$("#slider_img_container div:nth-child(0)").css("height", $(window).height())
	$("#slider_img_container div:nth-child(1)").css("height", $(window).height());
});

SliderView.prototype.initSliderListeners = function() {
	$("#slider_next").click(function() {
		$("#next-btn").click();
	});
	
	$("#slider_prev").click(function() {
		$("#prev-btn").click();
	});
	
	$("#slider_index div").each(function(i) {
		var index = i;
		$(this).click(function() {
			$(".thumbnails ul li")[index].click();
		});
	});
};

SliderView.prototype.unload = function() {
	$("#slider_next").click(function() {
		$("#next-btn").click();
	});
	
	$("#slider_prev").click(function() {
		$("#prev-btn").click();
	});
	
	$("#slider_index div").each(function(i) {
		var index = i;
		$(this).click(function() {
			$(".thumbnails ul li")[index].click();
		})
	});
};

var slider = new SliderView();