function Page() {
	var page = new _Page(), self = this, pageName, languageSubscribe, callBack;
	
	this.init = function(views, firstLoadViews, _pageName, _callBack) {
		page.init(views, firstLoadViews);
		pageName = _pageName;
		callBack = _callBack;
	};
	
	this.render = function(parameters) {
		page.render(parameters);
	};
	
	this.destroy = function() {
		page.destroy();
	};
		
	this.getPage = function() {
		return page;
	};
	
	this.setLanguageSubscribe = function(_languageSubscribe) {
		languageSubscribe = _languageSubscribe;
	};
	
	function _Page() {

		var views = [], self = this, subscribeFunc, parameters, firstLoadViews = [];
	
		this.init = function(_views, _firstLoadViews) {
			views = _views;
			// should be en every page
			views.push(topHeaderView);
			firstLoadViews = _firstLoadViews ? _firstLoadViews : [];
		};
	
		this.languageSubscribe = function(parameters) {
			subscribeFunc = function() {
				self.render(parameters, true);
				if(languageSubscribe) {
					languageSubscribe();
				}
			};
			$.subscribe("LanguageChanged", subscribeFunc);
		};
	
		this.render = function(_parameters, isLanguageChanged) {
			
			if(callBack) {
				callBack();
			}
			
			window.currentpage = pageName;
			parameters = _parameters;
			var _view;
			
			if (firstLoadViews.length > 0) {
				_view = firstLoadViews[0];
			} 
			render();
			
			function render() {
				renderView(_view).done(function() {
					$.each(views, function(index, view) {
						if(view != _view) {
							renderView(view);
						}
					});
				});
			};
			
			function renderView(view) {
				var deferred = $.Deferred();
				if(!view) {
					deferred.resolve({});
					return deferred;
				}
				
				if(!isLanguageChanged) {
					if(!view.dontRerenderOnPageChange) {
						view.render(parameters).done(function() {
							view.initilizeListeners(parameters);
							deferred.resolve({});
						});
					}
					return deferred;
				} else {
					if (!view.dontRenderOnLanguageChange ) {
						if (!view.dontReloadHtmlOnLanguageChange) {
							view.render(parameters).done(function() {
								view.initilizeListeners(parameters);
								deferred.resolve({});
							});
						} else {
							view.initilizeListeners(parameters);
							deferred.resolve({});
						}
					}else {
						deferred.resolve({});
					}
					
				}
				return deferred;
			}
		};
	
		this.destroy = function() {
			$.unsubscribe("LanguageChanged", subscribeFunc);
			subscribeFunc = null;
		};
	}
};