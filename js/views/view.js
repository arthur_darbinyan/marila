function View() {
	var templateId,
		parentElementId,
		self = this,
		isTemplateLoaded = false,
		templateHtml,
		templateDependsOnLanguage = false,
		languageDependTemplates = {"arm" : false, "ru": false, "en": false};
	
	this.isInitialized = false;
	this.parameters = {};
	
	this.initialize = function(args) {
		templateId = args.templateId;
		parentElementId = args.parentElementId;
		self.templateId = templateId;
		templateDependsOnLanguage = args.templateDependsOnLanguage;
		if('templateParameters' in args) {
			self.parameters = args.templateParameters;
		}
	};

	this.render = function() {
		var deferred = $.Deferred();
		
		if(templateDependsOnLanguage) {
			if(!languageDependTemplates[window.currentLanguage]) {
				self.loadTemplate().done(function(html) {
					$("#" + parentElementId).html(Mustache.to_html(html, {
		                data: self.parameters, 
		                lang: window.lang
		            }));
					deferred.resolve({});
				});
			} else {
				$("#" + parentElementId).html(Mustache.to_html(languageDependTemplates[window.currentLanguage], {
	                data: self.parameters, 
	                lang: window.lang
	            }));
				deferred.resolve({});
			}
			return deferred;
		}
		
		if(!isTemplateLoaded) {
			self.loadTemplate().done(function(html) {
				$("#" + parentElementId).html(Mustache.to_html(html, {
	                data: self.parameters, 
	                lang: window.lang
	            }));
				deferred.resolve({});
			});
		} else {
			$("#" + parentElementId).html(Mustache.to_html(templateHtml, {
	                data: self.parameters, 
	                lang: window.lang
	            }));
			deferred.resolve({});
		}
		return deferred;
	};

	this.loadTemplate = function() {
		var deferred = $.Deferred();
		var template = templateId;
		if(templateDependsOnLanguage) {
			template += "-" + window.currentLanguage;
		}

		$.ajax({
			url : "templates/" + template + '.html',
			success : function(template) {
				deferred.resolve(template);
				if(templateDependsOnLanguage) {
					languageDependTemplates[window.currentLanguage] = template;
				} else {
					isTemplateLoaded = true;
					templateHtml = template;
				}
			},
			dataType : "html"
		});
		
		return deferred;
	};
	
	this.getTemplateId = function() {
		return templateId;
	}
}