var homePage = new Page();
homePage.destroy = function() {
	$(window).unbind('resize.slider');
	$("#slider").remove();
	this.getPage().destroy();
	$(document.body).css("overflow", "visible")
};
homePage.init([menuView, sliderContentView], [sliderView], "home", function() {
		switch(window.currentLanguage) {
		case 'arm':
		window.document.title = "Marila | Կաթնամթերք, Մսամթերք";
		break;
		case 'en':
		window.document.title = "Marila | Milk Products, Meat Products";
		break;
		case 'ru':
		window.document.title = "Marila | Молочные Продукты, Мясные Продукты";
		break;
	}
});
homePage.setLanguageSubscribe(function() {
	$("#read_more").html(window.lang.Common.READMORE);
});
